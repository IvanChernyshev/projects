package common.equations.fa�tory;

import common.equations.*;
import common.utils.*;

public class QuadraticType7Creator extends EquationCreator {

	public Equation create() {
		int x1 = RandomGenerator.getRandom(5, 14);
		int x2 = RandomGenerator.getRandom(-4, -1);
		int a = RandomGenerator.getRandom(-9, -1);
		int b = -a * (x1 + x2);
		int c = a * (x1 * x2);
		String task = " ������ ���������: \n" + " " + a + "x^2 + " + b + "x + " + c + " = 0"
				+ "\n � ����� �������� ���������� ������.";
		return new Quadratic(a, b, c, 0, 0, 0, x1, x2, task);
	}

}
