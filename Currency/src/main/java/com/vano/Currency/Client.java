package com.vano.Currency;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;

public class Client {
	private JLabel currencyLabel;
	private JComboBox<String> comboBox;
	private TextHandler handler;
	private Vector<String> arrayOfCurrency;

	public static void main(String[] args) {
		new Client().start();
	}

	public Client() {
		this.handler = new TextHandler();
	}

	private void start() {
		try {
			handler.parseResponse();
			handler.parseXML();
			arrayOfCurrency = handler.createCurrencyTitles();
			setUpGui();
		} catch (UnsupportedOperationException ex) {
			emergencyExit();
		}
	}

	private void setUpGui() {
		JFrame frame = new JFrame("Currency converter");
		frame.setBounds(300, 100, 452, 200);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.getContentPane().setLayout(new BorderLayout(0, 0));

		MyPanel panel = new MyPanel();
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(null);

		JButton costButton = new JButton("Costs :");
		costButton.setBounds(313, 60, 121, 38);
		costButton.setBackground(Color.gray);
		costButton.setForeground(Color.green);
		costButton.setFont(new Font("Verdana", Font.ITALIC, 20));
		costButton.addActionListener(new TimeListener());
		panel.add(costButton);

		currencyLabel = new JLabel("Result of conversion");
		currencyLabel.setBounds(10, 109, 424, 52);
		currencyLabel.setForeground(Color.green);
		currencyLabel.setFont(new Font("Verdana", Font.ITALIC, 25));
		currencyLabel.setHorizontalAlignment(JLabel.CENTER);
		currencyLabel.setBackground(Color.gray);
		currencyLabel.setOpaque(true);
		panel.add(currencyLabel);

		comboBox = new JComboBox<String>(arrayOfCurrency);
		comboBox.setBounds(10, 60, 293, 38);
		comboBox.setForeground(Color.green);
		comboBox.setFont(new Font("Verdana", Font.ITALIC, 20));
		comboBox.setBackground(Color.gray);
		panel.add(comboBox);

		frame.revalidate();
		frame.repaint();
	}

	public class TimeListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			String currency = (String) comboBox.getSelectedItem();
			findData(currency);
		}
	}

	private void findData(String currency) {
		handler.findData(currency);
		updateData();
	}

	private void updateData() {
		int nominalInt = Integer.parseInt(handler.getNominal());
		double valueDouble = Double.parseDouble(handler.getValue().replace(",", "."));
		String result = String.format("%.2f", (valueDouble / nominalInt));
		currencyLabel.setText(result + " rubles");
	}

	private void emergencyExit() {
		JOptionPane.showMessageDialog(null, "Ошибка в чтении файла XML, попробуйте в другой раз");
		System.exit(0);
	}
}
