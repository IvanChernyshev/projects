package student.model;

import java.io.*;
import java.util.logging.*;
import javax.swing.*;

import student.main.*;
import common.constants.*;
import common.utils.dataObjects.*;

public class SaveModel extends AbstractModel {

	public SaveModel(UserData userData) {
		super(userData);
	}

	public void init(SaveDataObject dataObject) {
		JFileChooser chooser = new JFileChooser();
		chooser.showSaveDialog(dataObject.getPanel());
		saveText(chooser.getSelectedFile(), dataObject);
	}

	private void saveText(File file, SaveDataObject dataObject) {
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(file));
			String text = dataObject.getText();
			String[] mass = text.split(UtilConstants.SEPARATOR);
			for (String t : mass) {
				writer.write(t);
				writer.newLine();
			}
			writer.close();
		} catch (IOException ex) {
			Log.log(Level.SEVERE, "I/O error, method saveText()", ex);
		}
	}

}
