package teacher.main;

import java.util.*;

import common.utils.dataObjects.*;

public class UserData extends Observable {
	private static volatile UserData instance = null;
	private Person person;
	private Task task;
	private Result result;
	private Vector<Result> arrayOfResults;
	private boolean selfWorking, netWorking;
	private final Integer version = 1;

	private UserData() {
		arrayOfResults = new Vector<Result>();
	}

	public final static UserData getInstance() {
		if (instance == null) {
			synchronized (UserData.class) {
				if (instance == null) {
					instance = new UserData();
				}
			}
		}
		return instance;
	}

	public void dataChange() {
		setChanged();
		notifyObservers();
	}

	public void setPerson(Person person) {
		this.person = person;
		dataChange();
	}

	public Person getPerson() {
		return person;
	}

	public void setTask(Task task) {
		this.task = task;
		dataChange();
	}

	public Task getTask() {
		return task;
	}

	public void setResult(Result result) {
		this.result = result;
		dataChange();
	}

	public Result getResult() {
		return result;
	}

	public void setArrayOfResults(Vector<Result> arrayOfResults) {
		this.arrayOfResults = arrayOfResults;
		dataChange();
	}

	public void updateArrayOfResults() {
		dataChange();
	}

	public Vector<Result> getArrayOfResults() {
		return arrayOfResults;
	}

	public Integer getVersion() {
		return version;
	}

	public boolean isNetWorking() {
		return netWorking;
	}

	public void setNetWorking(boolean netWorking) {
		this.netWorking = netWorking;
		dataChange();
	}

	public boolean isSelfWorking() {
		return selfWorking;
	}

	public void setSelfWorking(boolean selfWorking) {
		this.selfWorking = selfWorking;
		dataChange();
	}
}
