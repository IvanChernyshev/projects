package com.vano.AnalogClock;


import java.awt.*;
import javax.swing.*;

public class ClockFrame extends JFrame {

	private static final long serialVersionUID = 1L;

	public ClockFrame(String name) {
		super(name);
		init();
	}

	private void init() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(500, 200, 407, 430);
		getContentPane().setLayout(new BorderLayout(0, 0));
		setResizable(false);
	}
}
