package common.utils.dataObjects;

import java.util.*;

import common.equations.*;

public class ResultDataObject {
	private final List<Equation> doneEquations;
	private final int numberOfTasks;

	public ResultDataObject(List<Equation> doneEquations, int numberOfTasks) {
		this.doneEquations = doneEquations;
		this.numberOfTasks = numberOfTasks;
	}

	public int getNumberOfTasks() {
		return numberOfTasks;
	}

	public List<Equation> getListOfDoneEquations() {
		return doneEquations;
	}
}
