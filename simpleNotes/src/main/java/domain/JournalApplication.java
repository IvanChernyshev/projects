package domain;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import repository.JournalRepository;


@SpringBootApplication
@ComponentScan(basePackages = {"domain", "repository", "web"})
@EntityScan("domain")
@EnableJpaRepositories("repository")
public class JournalApplication {
    public static void main(String[] args) {
        SpringApplication.run(JournalApplication.class, args);
    }

    @Bean
    InitializingBean saveData(JournalRepository repository) {
        return () -> {
            repository.save(new Journal("Wife's birthday", "Today is a birthday of my wife", "03/11/2017"));
            repository.save(new Journal("1st work day", "Today is 1st work day", "03/11/2017"));
            repository.save(new Journal("2nd work day", "Today is 2nd work day", "04/11/2017"));
            repository.save(new Journal("3th work day", "Today is 3th work day", "05/11/2017"));
            repository.save(new Journal("4th work day", "Today is 4th work day", "06/11/2017"));
        };
    }
}
