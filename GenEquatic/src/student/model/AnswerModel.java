package student.model;

import student.main.*;
import common.utils.dataObjects.*;

public class AnswerModel extends AbstractModel {

	public AnswerModel(UserData userData) {
		super(userData);
	}

	public void setTask(Task task) {
		synchronized (UserData.getInstance()) {
			userData.setTask(task);
		}
	}
}
