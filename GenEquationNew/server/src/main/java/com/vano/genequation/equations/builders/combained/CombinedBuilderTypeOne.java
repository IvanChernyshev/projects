package com.vano.genequation.equations.builders.combained;

import com.vano.genequation.equations.Equation;
import com.vano.genequation.utils.Const;

import java.util.UUID;

import static com.vano.genequation.utils.Const.SOLVE_EQUATION_EN;
import static com.vano.genequation.utils.Random.getRandomInt;

public class CombinedBuilderTypeOne extends CombinedBuilder {

    @Override
    public Equation build() {
        final int reply = getRandomInt(1, 11);
        final int a = getRandomInt(2, 7);
        final int c = getRandomInt(8, 13);
        final int b = -1 * (c * c + a * a + 2 * a * reply);
        return Equation.builder()
                .id(UUID.randomUUID())
                .equationType(Const.EquationType.COMBINED)
                .description(SOLVE_EQUATION_EN)
                .body("(x + " + a + ")^2 " + b + " = (x - " + c + ")(x + " + c + ")")
                .rightReply(reply)
                .build();
    }
}
