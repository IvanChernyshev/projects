package server;

import java.io.*;
import java.net.*;

public class StartConnection implements Runnable {
	private int numberOfConnection;
	private Server server;

	public StartConnection(Server server) {
		this.server = server;
	}

	public void run() {
		try {
			while (true) {
				Socket clientSocket = server.getServerSocket().accept();
				server.getArrayOfSockets().add(clientSocket);
				addNumberOfConnection();
				Thread singleClientThread = new Thread(new ServerHandler(clientSocket, server, this));
				singleClientThread.start();
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public synchronized void addNumberOfConnection() {
		numberOfConnection++;
		server.getConnectionLabel().setText("������������� ����������: " + numberOfConnection);
	}

	public synchronized void deleteNumberOfConnection() {
		numberOfConnection--;
		server.getConnectionLabel().setText("������������� ����������: " + numberOfConnection);
	}
}
