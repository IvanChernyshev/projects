package com.vano.myTime.model;

import com.vano.myTime.controller.*;

public class LaunchGui {
	public static void main(String[] args) {
		try {
			Controller.getInstance().init();
		} catch (UnsupportedOperationException ex) {
			System.exit(0);
		}
	}
}