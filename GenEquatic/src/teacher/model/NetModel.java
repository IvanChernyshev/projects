package teacher.model;

import java.io.*;
import java.net.*;
import java.util.logging.*;
import javax.swing.*;

import teacher.main.*;
import common.constants.*;
import common.utils.dataObjects.*;

public class NetModel extends AbstractModel {
	private Socket socket;
	ObjectOutputStream outputStream;

	public NetModel(UserData userData) {
		super(userData);
	}

	public void setUpNetworking() {
		try {
			socket = new Socket(userData.getPerson().getAddress(), UtilConstants.PORT);
			init();
			Thread handler = new Thread(new NetHandler(socket, userData, this));
			handler.start();
			userData.setNetWorking(true);
		} catch (ConnectException ex) {
			JOptionPane.showMessageDialog(null,
					"�� ������� ���������� ����������, ��������� ��������� �� ��������, ����� ��������� ������ �� ��������!!!");
			Log.log(Level.SEVERE, "Can not establish connection, remote server does not work", ex);
			userData.setNetWorking(false);
		} catch (IOException ex) {
			JOptionPane.showMessageDialog(null, "������ �����-������");
			Log.log(Level.SEVERE, "I/O error, method setUpNetworking()", ex);
			userData.setNetWorking(false);
		}
	}

	public void sendMessage() {
		try {
			synchronized (UserData.getInstance()) {
				Task task = userData.getTask();
				outputStream.writeObject(task);
			}
		} catch (IOException ex) {
			JOptionPane.showMessageDialog(null, "������ ����������, �� ������� �������� �������.");
			Log.log(Level.SEVERE, "Connection error, could not transmit the task", ex);
			end();
		}
	}

	private void init() {
		try {
			outputStream = new ObjectOutputStream(socket.getOutputStream());
			outputStream.writeObject(userData.getVersion());
		} catch (IOException ex) {
			JOptionPane.showMessageDialog(null, "������ �����-������");
			Log.log(Level.SEVERE, "I/O error, method init()", ex);
			end();
		}
	}

	public void end() {
		try {
			socket.close();
		} catch (IOException ex) {
			Log.log(Level.INFO, "Closing the socket (NetNodel.socket)", ex);
		}
	}
}
