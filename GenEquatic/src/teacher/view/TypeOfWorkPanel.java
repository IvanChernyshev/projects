package teacher.view;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import teacher.main.*;
import common.utils.*;
import teacher.controller.*;

public class TypeOfWorkPanel extends AbstractPanel {

	private JButton getButton;

	public TypeOfWorkPanel(UserData userData) {
		super(userData);
	}

	@Override
	protected void setStatic() {
		mainPanel = new JPanel();
		mainPanel.setBounds(0, 0, 384, 471);
		mainPanel.setLayout(null);
		mainPanel.setBackground(Color.lightGray);
		mainPanel.setBorder(TextHandler.getBorder2());

		JPanel topPanel = new JPanel();
		topPanel.setBounds(10, 11, 364, 54);
		mainPanel.add(topPanel);
		topPanel.setLayout(null);
		topPanel.setBackground(Color.lightGray);
		topPanel.setBorder(TextHandler.getBorder1());

		JLabel topLabel = new JLabel("�������� ��������");
		topLabel.setBounds(87, 11, 210, 32);
		topPanel.add(topLabel);
		topLabel.setFont(TextHandler.getBigFont());

		JPanel middlePanel = new JPanel();
		middlePanel.setBounds(10, 76, 364, 265);
		mainPanel.add(middlePanel);
		middlePanel.setLayout(null);
		middlePanel.setBackground(Color.lightGray);
		middlePanel.setBorder(TextHandler.getBorder1());
		ImageIcon icon = new ImageIcon(TypeOfWorkPanel.class.getResource("/teacher/se.jpeg"));
		JLabel iconLabel = new JLabel(icon);
		iconLabel.setBounds(1, 1, 362, 262);
		middlePanel.add(iconLabel);

		JPanel lowPanel = new JPanel();
		lowPanel.setBounds(10, 352, 364, 54);
		mainPanel.add(lowPanel);
		lowPanel.setLayout(null);
		lowPanel.setBackground(Color.lightGray);
		lowPanel.setBorder(TextHandler.getBorder1());

		JLabel lowLabel = new JLabel("������ �� ���������� ��� ����� �����");
		lowLabel.setBounds(40, 11, 334, 32);
		lowPanel.add(lowLabel);
		lowLabel.setFont(TextHandler.getLittleFont());
	}

	@Override
	protected void setDynamic() {
		getButton = new JButton("������");
		getButton.setBounds(10, 418, 121, 42);
		mainPanel.add(getButton);
		getButton.setFont(TextHandler.getLittleFont());
		getButton.addActionListener(new GetButtonListener());

		JButton selfButton = new JButton("����");
		selfButton.setBounds(253, 418, 121, 42);
		mainPanel.add(selfButton);
		selfButton.setFont(TextHandler.getLittleFont());
		selfButton.addActionListener(new SelfButtonListener());
	}

	public class GetButtonListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			TypeOfWorkController.getInstance().workingWithStudent();
		}
	}

	public class SelfButtonListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			TypeOfWorkController.getInstance().selfWorking();
		}
	}

	@Override
	public void updateView() {
		boolean changedMode = UserData.getInstance().isNetWorking();
		getButton.setEnabled(changedMode);
	}
}
