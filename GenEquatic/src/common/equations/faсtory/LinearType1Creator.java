package common.equations.fa�tory;

import common.equations.*;
import common.utils.*;

public class LinearType1Creator extends EquationCreator {

	public Equation create() {
		int x1 = RandomGenerator.getRandom(-15, 15);
		int a = RandomGenerator.getRandom(-10, -1);
		int b = RandomGenerator.getRandom(1, 10);
		int c = RandomGenerator.getRandom(-10, -1);
		int d = RandomGenerator.getRandom(1, 10);
		int f = x1 * a - b * (c + d * x1);
		String task = " ������ ���������: \n" + " " + a + "x - " + b + "(" + c + " + " + d + "x" + ") = " + f;
		return new Linear(a, b, c, d, f, 0, x1, 0, task);
	}
}
