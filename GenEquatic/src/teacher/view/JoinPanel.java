package teacher.view;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

import teacher.main.*;
import common.constants.*;
import common.utils.*;
import teacher.controller.*;

public class JoinPanel extends AbstractPanel {

	public JoinPanel(UserData userData) {
		super(userData);
	}

	@Override
	protected void setStatic() {
		mainPanel = new JPanel();
		mainPanel.setBounds(0, 0, 384, 471);
		mainPanel.setLayout(null);
		mainPanel.setBackground(Color.lightGray);
		mainPanel.setBorder(TextHandler.getBorder2());

		JPanel topPanel = new JPanel();
		topPanel.setBounds(10, 11, 364, 348);
		mainPanel.add(topPanel);
		topPanel.setLayout(null);
		topPanel.setBackground(Color.lightGray);
		topPanel.setBorder(TextHandler.getBorder1());

		JLabel firstLabel = new JLabel("�������, ��� ����������� ����������");
		firstLabel.setBounds(20, 11, 334, 94);
		topPanel.add(firstLabel);
		firstLabel.setFont(TextHandler.getMediumFont());

		JLabel secondLabel = new JLabel("GenEquatic");
		secondLabel.setBounds(123, 116, 118, 46);
		topPanel.add(secondLabel);
		secondLabel.setFont(TextHandler.getBigFont());

		JLabel verLabel = new JLabel("������ ������������� 1.2");
		verLabel.setBounds(88, 173, 197, 30);
		topPanel.add(verLabel);
		verLabel.setFont(TextHandler.getLittleFont());

		JLabel thirdLabel = new JLabel("����������� 'AmigoCompany'");
		thirdLabel.setBounds(10, 266, 344, 30);
		topPanel.add(thirdLabel);
		thirdLabel.setFont(TextHandler.getLittleFont());

		JLabel fourthLabel = new JLabel("������ �������� 2017");
		fourthLabel.setBounds(10, 307, 169, 30);
		topPanel.add(fourthLabel);
		fourthLabel.setFont(TextHandler.getLittleFont());
	}

	@Override
	protected void setDynamic() {
		JPanel lowPanel = new JPanel();
		lowPanel.setBounds(10, 370, 364, 90);
		mainPanel.add(lowPanel);
		lowPanel.setLayout(null);
		lowPanel.setBackground(Color.lightGray);
		lowPanel.setBorder(TextHandler.getBorder1());

		JButton enterButton = new JButton("�����");
		enterButton.setBounds(130, 21, 97, 47);
		lowPanel.add(enterButton);
		enterButton.setFont(TextHandler.getLittleFont());
		enterButton.addActionListener(new EnterButtonListener());
	}

	public class EnterButtonListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			CommonController.getInstance().selectPanel(PanelsConstants.LOGIN_PANEL);
		}
	}
}
