package teacher.view;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

import teacher.main.*;
import common.constants.*;
import common.equations.*;
import common.utils.*;
import common.utils.dataObjects.Result;
import common.utils.dataObjects.SaveDataObject;
import teacher.controller.*;

public class DetailPanel extends AbstractPanel {

	private JTextArea textArea;
	private Result result;

	public DetailPanel(UserData userData) {
		super(userData);
	}

	@Override
	protected void setStatic() {
		mainPanel = new JPanel();
		mainPanel.setBounds(0, 0, 384, 471);
		mainPanel.setLayout(null);
		mainPanel.setBackground(Color.lightGray);
		mainPanel.setBorder(TextHandler.getBorder2());

		JPanel namePanel = new JPanel();
		namePanel.setBounds(10, 11, 364, 56);
		mainPanel.add(namePanel);
		namePanel.setLayout(null);
		namePanel.setBackground(Color.lightGray);
		namePanel.setBorder(TextHandler.getBorder1());

		JLabel nameLabel = new JLabel("����������� ����������� �����");
		nameLabel.setBounds(21, 11, 333, 34);
		nameLabel.setFont(TextHandler.getBigFont());
		namePanel.add(nameLabel);

		JPanel textPanel = new JPanel();
		textPanel.setBounds(10, 78, 364, 305);
		mainPanel.add(textPanel);
		textPanel.setLayout(null);
		textPanel.setBackground(Color.lightGray);
		textPanel.setBorder(TextHandler.getBorder1());

		textArea = new JTextArea();
		textArea.setBounds(10, 11, 344, 283);
		textArea.setFont(TextHandler.getMediumFont());
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		textArea.setEditable(false);

		JScrollPane scroll = new JScrollPane(textArea);
		scroll.setBounds(10, 11, 344, 283);
		scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		scroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		textPanel.add(scroll);
	}

	@Override
	protected void setDynamic() {
		JButton saveButton = new JButton("���������");
		saveButton.setBounds(10, 394, 142, 43);
		saveButton.setFont(TextHandler.getLittleFont());
		mainPanel.add(saveButton);
		saveButton.addActionListener(new SaveActionListener());

		JButton closeButton = new JButton("�������");
		closeButton.setBounds(232, 394, 142, 43);
		closeButton.setFont(TextHandler.getLittleFont());
		mainPanel.add(closeButton);
		closeButton.addActionListener(new CloseActionListener());
	}

	public class SaveActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			SaveDataObject dataObject = new SaveDataObject(textArea.getText(), mainPanel);
			DetailController.getInstance().saveData(dataObject);
		}
	}

	public class CloseActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			DetailController.getInstance().closePanel();
		}
	}

	@Override
	public void update(Observable o, Object arg) {
		if (o instanceof UserData) {
			UserData userData = (UserData) o;
			this.result = userData.getResult();
		}
	}

	@Override
	public void updateView() {
		textArea.append(
				result.getDate() + "\n" + result.getPerson().getFirstName() + " " + result.getPerson().getSecondName()
						+ " " + result.getPerson().getGroup() + "\n" + UtilConstants.SEPARATOR + "\n");
		for (Equation equation : result.getDone()) {
			String detail = equation.toString();
			textArea.append(detail + "\n" + UtilConstants.SEPARATOR + "\n");
		}
	}

	@Override
	public void clearAll() {
		textArea.setText("");
	}
}