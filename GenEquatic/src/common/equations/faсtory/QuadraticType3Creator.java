package common.equations.fa�tory;

import common.equations.*;
import common.utils.*;

public class QuadraticType3Creator extends EquationCreator {

	public Equation create() {
		int x1 = RandomGenerator.getRandom(1, 8);
		int x2 = RandomGenerator.getRandom(9, 15);
		int b = -(x1 + x2);
		int c = (x1 * x2);
		String task = " ������ ���������: \n" + " " + "x^2 " + b + "x + " + c + " = 0"
				+ "\n � ����� �������� ���������� ������.";
		return new Quadratic(0, b, c, 0, 0, 0, x1, x2, task);
	}
}
