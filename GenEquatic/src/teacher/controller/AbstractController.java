package teacher.controller;

import java.util.*;
import java.util.logging.*;
import teacher.view.*;

public abstract class AbstractController {
	protected MainFrame frame;
	protected List<AbstractPanel> panels;
	protected Logger Log;

	protected AbstractController() {
		Log = Logger.getLogger(this.getClass().getName());
	}

	public void selectPanel(int numOfView) {
		frame.deletePanel();
		frame.setPanel(panels.get(numOfView));
		frame.repaint();
	}

	public void clearPanel(int numOfView) {
		panels.get(numOfView).clearAll();
	}

	public void init(MainFrame frame, List<AbstractPanel> panels) {
		setFrame(frame);
		setPanels(panels);
	}

	public void setFrame(MainFrame frame) {
		this.frame = frame;
	}

	public void setPanels(List<AbstractPanel> panels) {
		this.panels = panels;
	}

}
