package com.vano.RevelationServer;

import java.io.*;
import java.net.*;
import java.util.*;

import javax.swing.JOptionPane;

public class ServerHandler implements Runnable {
	private ObjectOutputStream outputStream;
	private String chapter;

	public ServerHandler(Socket socket) {
		try {
			this.outputStream = new ObjectOutputStream(socket.getOutputStream());
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Ошибка при создании объектов вывода");
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		try {
			outputStream.writeObject(randomString());
			outputStream.writeObject(chapter);
			chapter = null;
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Ошибка при передаче объекта String");
			e.printStackTrace();
		} finally {
			try {
				outputStream.close();
			} catch (IOException e) {
				JOptionPane.showMessageDialog(null, "Ошибка при закрытии объекта вывода");
				e.printStackTrace();
			}
		}
	}

	private String randomString() {
		ArrayList<ArrayList<String>> books = MyText.getInstance().getBooks();
		int chapter = new Random().nextInt(books.size());
		int verse = books.get(chapter).indexOf(books.get(chapter).get(new Random().nextInt(books.get(chapter).size())));
		String text = books.get(chapter).get(verse);
		setChapter(chapter, verse);
		return text;
	}

	private void setChapter(int chapter, int verse) {
		this.chapter = "Глава: " + (chapter + 1) + " " + "Стих: " + (verse + 1);
	}
}
