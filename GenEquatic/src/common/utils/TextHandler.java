package common.utils;

import java.awt.*;
import javax.swing.border.*;

public class TextHandler {

	public static final String FONT = "TimesRoman";

	public static Border getBorder1() {
		return new LineBorder(Color.yellow);
	}

	public static Border getBorder2() {
		return new EtchedBorder();
	}

	public static Font getLittleFont() {
		return new Font(FONT, Font.ITALIC, 14);

	}

	public static Font getMediumFont() {
		return new Font(FONT, Font.ITALIC, 17);
	}

	public static Font getBigFont() {
		return new Font(FONT, Font.BOLD, 20);
	}

	public static Font getSpetialFont() {
		return new Font(FONT, Font.ITALIC, 20);
	}
}
