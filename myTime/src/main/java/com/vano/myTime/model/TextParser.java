package com.vano.myTime.model;

import java.util.*;

class TextParser {
	private StringBuilder resultText;
	private List<String> splitters;
	private String result;

	 TextParser(String text) {
		this.resultText = new StringBuilder(text);
		init();
	}

	private void init() {
		splitters = new ArrayList<>();
		splitters.add("\\*");
		splitters.add("===");
		splitters.add("==");
		splitters.add("=");
	}

	 void parseText() {
		for (String regex : splitters) {
			parseText(resultText.toString(), regex);
		}
		result = reduceString();
	}

	private void parseText(String text, String regex) {
		StringBuilder builder = new StringBuilder();
		String[] textArray = text.split(regex);
		for (String string : textArray) {
			builder.append(string).append("\n");
		}
		resultText = builder;
	}

	private String reduceString() {
		String result = resultText.toString().replace("<onlyinclude>", "").replace("/", "");
		return result.replaceAll("\\{\\{[\\s\\S]*?\\}\\}", "");
	}

	String getResult() {
		return result;
	}
}
