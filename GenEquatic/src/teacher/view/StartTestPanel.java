package teacher.view;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.util.Timer;

import teacher.main.*;
import common.utils.*;
import common.utils.dataObjects.*;
import teacher.controller.*;

public class StartTestPanel extends AbstractPanel {
	private JPanel topPanel, lowPanel;
	private JLabel remainTimeLabel, doneTaskLabel, remainTaskLabel, timerLabel, nameLabel, taskLabel, replyLabel;
	private JTextField replyField;
	private JButton nextButton, endButton;
	private boolean firstClick = true;
	private int seconds, time, secondsDone;
	private Task task;
	private Timer timer;

	public StartTestPanel(UserData userData) {
		super(userData);
	}

	@Override
	protected void setStatic() {
		mainPanel = new JPanel();
		mainPanel.setBounds(0, 0, 384, 461);
		mainPanel.setLayout(null);
		mainPanel.setBackground(Color.lightGray);
		mainPanel.setBorder(TextHandler.getBorder2());

		topPanel = new JPanel();
		topPanel.setBounds(10, 11, 364, 95);
		topPanel.setBackground(Color.lightGray);
		topPanel.setBorder(TextHandler.getBorder1());
		mainPanel.add(topPanel);
		topPanel.setLayout(null);

		remainTimeLabel = new JLabel("�������� �������, �����");
		remainTimeLabel.setBounds(10, 11, 194, 23);
		remainTimeLabel.setFont(TextHandler.getLittleFont());
		remainTimeLabel.setVisible(false);
		topPanel.add(remainTimeLabel);

		lowPanel = new JPanel();
		lowPanel.setBounds(10, 117, 364, 241);
		lowPanel.setBackground(Color.lightGray);
		lowPanel.setBorder(TextHandler.getBorder1());
		mainPanel.add(lowPanel);
		lowPanel.setLayout(null);

		replyLabel = new JLabel("�����:");
		replyLabel.setBounds(10, 179, 75, 27);
		replyLabel.setFont(TextHandler.getMediumFont());
		replyLabel.setVisible(false);
		lowPanel.add(replyLabel);

		replyField = new JTextField("");
		replyField.setBounds(110, 180, 75, 24);
		lowPanel.add(replyField);
		replyField.setColumns(10);
		replyField.setVisible(false);
		replyField.setHorizontalAlignment(JTextField.CENTER);
		replyField.setFont(TextHandler.getMediumFont());
	}

	@Override
	protected void setDynamic() {

		doneTaskLabel = new JLabel();
		doneTaskLabel.setBounds(10, 61, 140, 23);
		doneTaskLabel.setFont(TextHandler.getLittleFont());
		topPanel.add(doneTaskLabel);

		remainTaskLabel = new JLabel();
		remainTaskLabel.setBounds(214, 61, 140, 23);
		remainTaskLabel.setFont(TextHandler.getLittleFont());
		topPanel.add(remainTaskLabel);

		timerLabel = new JLabel("~");
		timerLabel.setBounds(247, 9, 72, 23);
		timerLabel.setFont(TextHandler.getBigFont());
		timerLabel.setVisible(false);
		topPanel.add(timerLabel);

		nextButton = new JButton("�����");
		nextButton.setBounds(30, 380, 137, 48);
		nextButton.setFont(TextHandler.getMediumFont());
		mainPanel.add(nextButton);
		nextButton.addActionListener(new NextButtonActionListener());

		endButton = new JButton("���������");
		endButton.setBounds(216, 380, 137, 48);
		endButton.setFont(TextHandler.getMediumFont());
		mainPanel.add(endButton);
		endButton.addActionListener(new EndButtonActionListener());
		endButton.setEnabled(false);

		nameLabel = new JLabel();
		nameLabel.setBounds(118, 11, 143, 46);
		nameLabel.setFont(TextHandler.getBigFont());
		nameLabel.setVisible(false);
		lowPanel.add(nameLabel);

		taskLabel = new JLabel("   ����� ���������� ������� '�����'");
		taskLabel.setBounds(10, 68, 344, 91);
		taskLabel.setFont(TextHandler.getMediumFont());
		lowPanel.add(taskLabel);
	}

	public class NextButtonActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent ev) {
			if (firstClick) {
				firstClickAction();
			} else {
				start();
			}
		}
	}

	public class EndButtonActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent ev) {
			StartTestController.getInstance().testStop();
		}
	}

	private void start() {
		String userResponse = replyField.getText();
		StartTestController.getInstance().testStart(userResponse);
	}

	@Override
	public void updateView() {
		doneTaskLabel.setText("���������: " + TestData.getInstance().getDoneEquations());
		remainTaskLabel.setText("��������: " + TestData.getInstance().getRemainEquations());
		nameLabel.setText("������� " + TestData.getInstance().getCurrentEquation().getNumber());
		taskLabel.setText("<html>" + TestData.getInstance().getCurrentEquation().getTask());
		endButton.setEnabled(TestData.getInstance().isNewEquationsFull());
	}

	@Override
	public void update(Observable o, Object arg) {
		if (o instanceof UserData) {
			UserData userData = (UserData) o;
			this.task = userData.getTask();
		}
	}

	private void firstClickAction() {
		init();
		remainTimeLabel.setVisible(true);
		timerLabel.setVisible(true);
		replyLabel.setVisible(true);
		replyField.setVisible(true);
		nextButton.setText("���������");
		nameLabel.setVisible(true);
		firstClick = false;
		updateView();
	}

	@Override
	public void end() {
		doneTaskLabel.setText("���������: " + TestData.getInstance().getDoneEquations());
		remainTaskLabel.setText("��������: " + TestData.getInstance().getRemainEquations());
		nameLabel.setText("���������!");
		taskLabel.setVisible(false);
		replyLabel.setVisible(false);
		replyField.setVisible(false);
		nextButton.setEnabled(false);
		endButton.setEnabled(true);
		timerLabel.setText("~");
		timer.cancel();
	}

	private void init() {
		time = task.getTime();
		runTimer(time);
		StartTestController.getInstance().initModel();
	}

	@Override
	public void clear() {
		replyField.setText("");
		replyField.requestFocus();
	}

	@Override
	public void clearAll() {
		this.firstClick = true;
		this.seconds = 0;
		this.secondsDone = 0;
		this.time = 0;
		this.task = null;
		replyLabel.setVisible(false);
		replyField.setVisible(false);
		replyField.setText("");
		doneTaskLabel.setText("");
		remainTaskLabel.setText("");
		remainTimeLabel.setVisible(false);
		timerLabel.setText("~");
		timerLabel.setVisible(false);
		nameLabel.setText("");
		nameLabel.setVisible(false);
		taskLabel.setText("   ����� ���������� ������� '�����'");
		taskLabel.setVisible(true);
		nextButton.setEnabled(true);
		nextButton.setText("�����");
		endButton.setEnabled(false);
	}

	private void runTimer(int time) {
		timer = new Timer();
		TimerTask taskOfTimer = null;
		if (time > 0) {
			this.seconds = time * 60;
			taskOfTimer = new TimerWithTime();
		} else {
			taskOfTimer = new DefaultTimer();
		}
		timer.schedule(taskOfTimer, 0, 1000);
	}

	public class TimerWithTime extends TimerTask {

		@Override
		public void run() {
			if (seconds > 0) {
				synchronized (StartTestPanel.this) {
					seconds--;
					secondsDone++;
				}
				int minutesForView = seconds / 60;
				int secondsForView = seconds % 60;
				updateTime(minutesForView, secondsForView);
			} else {
				StartTestPanel.this.end();
			}
		}
	}

	public class DefaultTimer extends TimerTask {

		@Override
		public void run() {
			secondsDone++;
			synchronized (TestData.getInstance()) {
				TestData.getInstance().setSecondsDone(secondsDone);
			}
		}
	}

	private void updateTime(int minutesForView, int secondsForView) {
		timerLabel.setText(minutesForView + ":" + secondsForView);
		synchronized (TestData.getInstance()) {
			TestData.getInstance().setSecondsDone(secondsDone);
		}
	}

	@Override
	public JPanel getPanel() {
		return mainPanel;
	}
}
