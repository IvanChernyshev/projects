package com.vano.AnalogClock;


import java.awt.*;
import java.awt.image.*;
import javax.swing.*;

public class ClockPanel extends JPanel {
	private int primary_X = 200;
	private int primary_Y = 199;
	private int end_X_second;
	private int end_Y_second;
	private int end_X_minute;
	private int end_Y_minute;
	private int end_X_hour;
	private int end_Y_hour;
	private BufferedImage bufferedImage;
	private AlphaComposite alphaComposite;
	private ImageIcon icon;

	private static final long serialVersionUID = 1L;

	public ClockPanel(ImageIcon icon) {
		this.icon = icon;
		init();
	}

	private void init() {
		this.bufferedImage = new BufferedImage(400, 400, BufferedImage.TYPE_INT_ARGB);
		this.alphaComposite = AlphaComposite.getInstance(AlphaComposite.SRC);
	}

	private void drawBackground(Graphics2D g2d) {
		g2d.drawImage(icon.getImage(), 0, 0, null);
	}

	private void drawSeconds(Graphics2D g2d) {
		g2d.setColor(Color.RED);
		g2d.setStroke(new BasicStroke(2));
		synchronized (this) {
			g2d.drawLine(primary_X, primary_Y, end_X_second, end_Y_second);
		}
	}

	private void drawMinutes(Graphics2D g2d) {
		g2d.setColor(Color.BLACK);
		g2d.setStroke(new BasicStroke(4));
		synchronized (this) {
			g2d.drawLine(primary_X, primary_Y, end_X_minute, end_Y_minute);
		}
	}

	private void drawHours(Graphics2D g2d) {
		g2d.setColor(Color.BLACK);
		g2d.setStroke(new BasicStroke(6));
		synchronized (this) {
			g2d.drawLine(primary_X, primary_Y, end_X_hour, end_Y_hour);
		}
	}

	@Override
	public void paintComponent(Graphics g) {
		Graphics2D g2 = (Graphics2D) g;
		Graphics2D g2d = bufferedImage.createGraphics();
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		drawBackground(g2d);
		drawSeconds(g2d);
		drawMinutes(g2d);
		drawHours(g2d);
		g2d.setComposite(alphaComposite);
		g2.drawImage(bufferedImage, null, 0, 0);
	}

	public void setEnd_X_hour(int end_X_hour) {
		this.end_X_hour = end_X_hour;
	}

	public void setEnd_Y_hour(int end_Y_hour) {
		this.end_Y_hour = end_Y_hour;
	}

	public void setEnd_X_second(int end_X_second) {
		this.end_X_second = end_X_second;
	}

	public void setEnd_Y_second(int end_Y_second) {
		this.end_Y_second = end_Y_second;
	}

	public void setEnd_X_minute(int end_X_minute) {
		this.end_X_minute = end_X_minute;
	}

	public void setEnd_Y_minute(int end_Y_minute) {
		this.end_Y_minute = end_Y_minute;
	}
}
