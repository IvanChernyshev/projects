package common.equations.fa�tory;

import common.equations.*;
import common.utils.*;

public class FractionalRationalType3Creator extends EquationCreator {

	public Equation create() {
		int x1 = RandomGenerator.getRandom(1, 11);
		int a = RandomGenerator.getRandom(2, 7);
		int c = RandomGenerator.getRandom(8, 13);
		int b = x1 * (c - a);
		String task = " ������ ���������: \n" + " " + "(x^2 + " + a + "x + " + b + ")/(x + " + c + ") = x";
		return new FractionalRational(a, b, c, 0, 0, 0, x1, 0, task);
	}
}
