package student.controller;

import student.main.*;
import common.constants.*;
import common.utils.dataObjects.*;
import student.model.*;

public class LoginController extends AbstractController {

	private static volatile LoginController instance = null;

	private LoginController() {
	}

	public final static LoginController getInstance() {
		if (instance == null) {
			synchronized (LoginController.class) {
				if (instance == null) {
					instance = new LoginController();
				}
			}
		}
		return instance;
	}

	public void startWorking(LoginDataObject dataObject) {
		createPerson(dataObject);
		NetController.getInstance().setUpNetWorking();
		selectPanel(PanelsConstants.TYPE_OF_WORK_PANEL);
		clearPanel(PanelsConstants.LOGIN_PANEL);
	}

	private void createPerson(LoginDataObject dataObject) {
		LoginModel model = new LoginModel(UserData.getInstance());
		model.createPerson(dataObject);
	}
}
