package com.vano.AnalogClock;

import java.awt.BorderLayout;

import javax.swing.*;

public class AnalogClock {
	public static void main(String[] args) throws InterruptedException {
		ImageIcon icon = new ImageIcon(AnalogClock.class.getResource("/image/clock.jpg"));
		ClockFrame frame = new ClockFrame("AnalogClock");
		ClockPanel panel = new ClockPanel(icon);
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		new TimerClock(panel).runTimer();
		frame.setVisible(true);
	}
}
