package teacher.main;

import java.io.*;
import java.net.*;
import java.util.logging.*;
import javax.swing.*;

import common.utils.dataObjects.*;
import teacher.model.*;

public class NetHandler implements Runnable {
	private final Socket socket;
	private final UserData userData;
	private final NetModel netModel;
	private final Logger LOG = Logger.getLogger(NetHandler.class.getName());

	public NetHandler(Socket socket, UserData userData, NetModel netModel) {
		this.socket = socket;
		this.userData = userData;
		this.netModel = netModel;
	}

	public void run() {
		Result result = null;
		try {
			ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());
			while ((result = (Result) inputStream.readObject()) != null) {
				synchronized (UserData.getInstance()) {
					userData.getArrayOfResults().add(result);
					userData.updateArrayOfResults();
				}
			}
		} catch (IOException ex) {
			JOptionPane.showMessageDialog(null, "�� ������� �������� ������ result � ���������� ����������");
			LOG.log(Level.SEVERE, "couldn't obtain the Result object from remote server", ex);
		} catch (ClassNotFoundException ex) {
			LOG.log(Level.SEVERE, "Instead Result object was obtained another object", ex);
		} finally {
			netModel.end();
		}
	}
}