package com.vano.myTime.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import javax.swing.*;

import com.vano.myTime.controller.*;
import com.vano.myTime.model.*;

public class MainFrame {
    private final UserData userData;
    private JCheckBox beforeCheckBox;
    private JComboBox<String> firstComboBox, secondComboBox;
    private JLabel resultLabel;
    private JTextField textField;

    public MainFrame(UserData userData) {
        this.userData = userData;
    }

    public void start() {
        JFrame frame = new JFrame("TimeMachine");
        frame.setVisible(true);
        frame.setBounds(100, 200, 400, 400);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(new BorderLayout(0, 0));

        Panel panel = new Panel();
        frame.getContentPane().add(panel, BorderLayout.CENTER);
        panel.setLayout(null);

        JLabel topLabel = new JLabel("Change year :");
        topLabel.setBounds(10, 11, 364, 40);
        topLabel.setForeground(Color.green);
        topLabel.setFont(new Font("Verdana", Font.ITALIC, 25));
        topLabel.setHorizontalAlignment(JLabel.CENTER);
        topLabel.setBackground(Color.gray);
        panel.add(topLabel);

        textField = new JTextField();
        textField.setBounds(10, 62, 157, 40);
        textField.setForeground(Color.green);
        textField.setFont(new Font("Verdana", Font.ITALIC, 25));
        textField.setBackground(Color.gray);
        textField.setHorizontalAlignment(JLabel.CENTER);
        panel.add(textField);
        textField.setColumns(10);

        JLabel fromLabel = new JLabel("From :");
        fromLabel.setBounds(10, 113, 99, 40);
        fromLabel.setForeground(Color.green);
        fromLabel.setFont(new Font("Verdana", Font.ITALIC, 25));
        fromLabel.setBackground(Color.gray);
        panel.add(fromLabel);

        JLabel toLabel = new JLabel("To :");
        toLabel.setBounds(10, 164, 99, 40);
        toLabel.setForeground(Color.green);
        toLabel.setFont(new Font("Verdana", Font.ITALIC, 25));
        toLabel.setBackground(Color.gray);
        panel.add(toLabel);

        firstComboBox = new JComboBox<>(userData.getListOfAges());
        firstComboBox.setBounds(119, 115, 255, 40);
        firstComboBox.setForeground(Color.green);
        firstComboBox.setFont(new Font("Verdana", Font.ITALIC, 20));
        firstComboBox.setBackground(Color.gray);
        panel.add(firstComboBox);

        secondComboBox = new JComboBox<>(userData.getListOfAges());
        secondComboBox.setBounds(119, 166, 255, 40);
        secondComboBox.setForeground(Color.green);
        secondComboBox.setFont(new Font("Verdana", Font.ITALIC, 20));
        secondComboBox.setBackground(Color.gray);
        panel.add(secondComboBox);

        resultLabel = new JLabel();
        resultLabel.setBounds(10, 268, 364, 40);
        resultLabel.setForeground(Color.green);
        resultLabel.setFont(new Font("Verdana", Font.ITALIC, 25));
        resultLabel.setBackground(Color.gray);
        resultLabel.setHorizontalAlignment(JLabel.CENTER);
        resultLabel.setOpaque(true);
        panel.add(resultLabel);

        JButton okButton = new JButton("Change");
        okButton.setBounds(119, 217, 157, 40);
        okButton.addActionListener(e -> {
            String firstAge = (String) firstComboBox.getSelectedItem();
            synchronized (userData) {
                userData.setFirstAge(firstAge);
            }
            String secondAge = (String) secondComboBox.getSelectedItem();
            String year = textField.getText();
            boolean beforeAge = beforeCheckBox.isSelected();
            Controller.getInstance().calculateAge(firstAge, secondAge, year, beforeAge);
            update();
        });
        okButton.setBackground(Color.gray);
        okButton.setForeground(Color.green);
        okButton.setFont(new Font("Verdana", Font.ITALIC, 20));
        panel.add(okButton);

        JButton detailButton = new JButton("Details");
        detailButton.setBounds(119, 319, 157, 31);
        detailButton.addActionListener(e -> Controller.getInstance().showDetails());
        detailButton.setBackground(Color.gray);
        detailButton.setForeground(Color.green);
        detailButton.setFont(new Font("Verdana", Font.ITALIC, 20));
        panel.add(detailButton);

        beforeCheckBox = new JCheckBox("Before age");
        beforeCheckBox.setBounds(217, 58, 157, 40);
        beforeCheckBox.setForeground(Color.green);
        beforeCheckBox.setFont(new Font("Verdana", Font.ITALIC, 20));
        beforeCheckBox.setOpaque(false);
        panel.add(beforeCheckBox);

        frame.setResizable(false);
        frame.revalidate();
        frame.repaint();
    }

    private void update() {
        synchronized (userData) {
            resultLabel.setText(userData.getResult());
        }
    }

//    public void clearAll() {
//        beforeCheckBox.setSelected(false);
//        resultLabel.setText("");
//        textField.setText("");
//    }
}
