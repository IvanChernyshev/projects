package common.constants;

public final class PanelsConstants {
	public static final int JOIN_PANEL = 0;
	public static final int LOGIN_PANEL = 1;
	public static final int TYPE_OF_WORK_PANEL = 2;
	public static final int ANSWER_PANEL = 3;
	public static final int TASK_PANEL = 4;
	public static final int START_TEST_PANEL = 5;
	public static final int SAVE_TASK_PANEL = 6;
	public static final int RESULT_PANEL = 7;
	public static final int DETAIL_PANEL = 8;
}
