package student.controller;

import student.main.*;
import student.model.*;

public class NetController extends AbstractController {
	private static volatile NetController instance = null;
	private NetModel model;

	private NetController() {
	}

	public final static NetController getInstance() {
		if (instance == null) {
			synchronized (NetController.class) {
				if (instance == null) {
					instance = new NetController();
				}
			}
		}
		return instance;
	}

	public void setUpNetWorking() {
		this.model = new NetModel(UserData.getInstance());
		model.setUpNetworking();
	}

	public void sendMessage() {
		model.sendMessage();
	}
}
