package com.vano.genequation.equations;

import com.vano.genequation.utils.Const;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Equation {

    @Id
    private UUID id;
    @Enumerated(EnumType.STRING)
    private Const.EquationType equationType;
    private String description;
    private String body;
    private long userReply;
    private long rightReply;
    private boolean isResolved;
    private boolean isResolvedCorrectly;
    private long sequenceNumber;

    public boolean compareReply() {
        return isResolvedCorrectly = rightReply == userReply;
    }
}
