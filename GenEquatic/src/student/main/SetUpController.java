package student.main;

import java.util.*;

import student.controller.*;
import student.view.*;

public class SetUpController {

	public void initControllers(MainFrame frame, List<AbstractPanel> panels) {
		LoginController.getInstance().init(frame, panels);
		TypeOfWorkController.getInstance().init(frame, panels);
		TaskController.getInstance().init(frame, panels);
		StartTestController.getInstance().init(frame, panels);
		CommonController.getInstance().init(frame, panels);
		AnswerController.getInstance().init(frame, panels);
		NetController.getInstance().init(frame, panels);
		DetailController.getInstance().init(frame, panels);
		SaveController.getInstance().init(frame, panels);
	}
}
