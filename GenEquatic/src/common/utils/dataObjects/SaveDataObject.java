package common.utils.dataObjects;

import javax.swing.*;

public class SaveDataObject {
	private final String text;
	private final JPanel panel;

	public SaveDataObject(String text, JPanel panel) {
		this.text = text;
		this.panel = panel;
	}

	public String getText() {
		return text;
	}

	public JPanel getPanel() {
		return panel;
	}
}
