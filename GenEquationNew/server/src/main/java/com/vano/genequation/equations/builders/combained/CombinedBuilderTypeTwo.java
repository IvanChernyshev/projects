package com.vano.genequation.equations.builders.combained;

import com.vano.genequation.equations.Equation;
import com.vano.genequation.utils.Const;

import java.util.UUID;

import static com.vano.genequation.utils.Const.SOLVE_EQUATION_EN;
import static com.vano.genequation.utils.Random.getRandomInt;

public class CombinedBuilderTypeTwo extends CombinedBuilder {

    @Override
    public Equation build() {
        final int reply = getRandomInt(-10, -1);
        final int a = getRandomInt(2, 7);
        final int c = getRandomInt(2, 8);
        final int b = c * c - reply;
        return Equation.builder()
                .id(UUID.randomUUID())
                .equationType(Const.EquationType.COMBINED)
                .description(SOLVE_EQUATION_EN)
                .body("x^2 - (" + a + "x + " + c + ")(" + a + "x - " + c + ") = x + " + b)
                .rightReply(reply)
                .build();
    }
}
