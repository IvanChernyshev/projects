package com.vano.myTime.model;

import java.io.*;
import java.util.*;
import javax.swing.*;

public class TextHandler {
    private final UserData userData;

    public TextHandler(UserData userData) {
        this.userData = userData;
    }

    public void initAge() throws UnsupportedOperationException {
        ArrayList<String> strings = new ArrayList<>();
        String text;
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(TextHandler.class.getResourceAsStream("/util/Ages.txt")))) {
            while ((text = reader.readLine()) != null) {
                strings.add(text);
            }
            splitStrings(strings);
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, "Can not read file \"Ages.txt\"");
            e.printStackTrace();
        }
    }

    private void splitStrings(ArrayList<String> strings) {
        HashMap<String, Integer> ages = new HashMap<>();
        Vector<String> listOfAges = new Vector<>();
        for (String st : strings) {
            String[] mass = st.split(",");
            try {
                listOfAges.add(mass[0]);
                int age = Integer.parseInt(mass[1]);
                ages.put(mass[0], age);
            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(null, "Can not read file \"Ages.txt\"");
                throw new UnsupportedOperationException();
            }
        }
        synchronized (userData) {
            userData.setAges(ages);
            userData.setListOfAges(listOfAges);
        }
    }

    public int validateUserInput(String string) throws UnsupportedOperationException {
        int result;
        try {
            result = Integer.parseInt(string);
            return result;
        } catch (NumberFormatException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "User set up year incorrectly");
            throw new UnsupportedOperationException();
        }
    }

    String localDescription(String age) {
        StringBuilder builder = new StringBuilder();
        String text;
        try (BufferedReader reader = new BufferedReader(
                new InputStreamReader(TextHandler.class.getResourceAsStream("/text/" + age + ".txt")))) {
            while ((text = reader.readLine()) != null) {
                builder.append(text);
            }
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, "Can not read file /text/" + age + ".txt");
            e.printStackTrace();
        }
        return builder.toString();
    }
}
