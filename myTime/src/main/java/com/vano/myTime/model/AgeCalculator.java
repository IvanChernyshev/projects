package com.vano.myTime.model;

import java.util.*;

public class AgeCalculator {
    private final UserData userData;
    private static final int BC=0;

    public AgeCalculator(UserData userData) {
        this.userData = userData;
    }

    public void calculate(String firstAge, String secondAge, int year, boolean beforeAge) {
        int ageToChrist, first_age, second_age;
        synchronized (userData) {
            first_age = userData.getAges().get(firstAge);
            second_age = userData.getAges().get(secondAge);
        }
        Calendar calendar = getAgeTiChrist(first_age, year, beforeAge);
        ageToChrist = calendar.get(Calendar.YEAR);
        if (calendar.get(Calendar.ERA) == BC) {
            ageToChrist = Math.negateExact(ageToChrist);
        }
        synchronized (userData) {
            userData.setAgeToChrist(ageToChrist);
        }
        calendar.add(Calendar.YEAR, -second_age);
        updateLabel(calendar);
    }

    private void updateLabel(Calendar calendar) {
        String afterBefore = "Before age ";
        if (calendar.get(Calendar.ERA) > 0) {
            afterBefore = "";
        }
        String result = (" " + afterBefore + " " + (Math.abs(calendar.get(Calendar.YEAR))) + " year");
        synchronized (userData) {
            userData.setResult(result);
        }
    }

    private Calendar getAgeTiChrist(int firstAge, int year, boolean before) {
        Calendar calendar = new GregorianCalendar(firstAge, 0, 1);
        if (before) {
            calendar.add(Calendar.YEAR, -year + 1);
        } else {
            calendar.add(Calendar.YEAR, year);
        }
        return calendar;
    }
}
