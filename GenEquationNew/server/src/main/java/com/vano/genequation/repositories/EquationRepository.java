package com.vano.genequation.repositories;

import com.vano.genequation.equations.Equation;
import com.vano.genequation.utils.Const;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface EquationRepository extends JpaRepository<Equation, UUID> {

    List<Equation> findAllByEquationType(Const.EquationType equationType);
}
