package com.vano.AnalogClock;


import java.awt.Point;
import java.time.LocalTime;
import java.util.Timer;
import java.util.TimerTask;

public class TimerClock extends TimerTask {
	private ClockPanel panel;

	public TimerClock(ClockPanel panel) {
		this.panel = panel;
	}

	public void runTimer() {
		Timer timer = new Timer();
		timer.schedule(this, 0, 50);
	}

	@Override
	public void run() {
		findTime();
		panel.repaint();
	}

	private void findTime() {
		LocalTime time = LocalTime.now();
		calculateSecond(time);
		calculateMinute(time);
		calculateHour(time);
	}

	private void calculateSecond(LocalTime time) {
		int radius = (Math.min(panel.getWidth(), panel.getHeight()) / 2) - 20;
		double angle = (Math.PI / 2 + time.getSecond() * 6.0 * Math.PI / 180);
		Point point = getEndPoint(angle, radius);
		synchronized (panel) {
			panel.setEnd_X_second((int) point.getX());
			panel.setEnd_Y_second((int) point.getY());
		}
	}

	private void calculateMinute(LocalTime time) {
		int radius = (Math.min(panel.getWidth(), panel.getHeight()) / 2) - 20;
		double angle = Math.PI / 2 + 6.0 * Math.PI / 180 * (time.getMinute() + time.getSecond() / 60.0);
		Point point = getEndPoint(angle, radius);
		synchronized (panel) {
			panel.setEnd_X_minute((int) point.getX());
			panel.setEnd_Y_minute((int) point.getY());
		}
	}

	private void calculateHour(LocalTime time) {
		int radius = (Math.min(panel.getWidth(), panel.getHeight()) / 2) - 85;
		double angle = Math.PI / 2 + 30.0 * Math.PI / 180 * (time.getHour() + time.getMinute() / 60.0);
		Point point = getEndPoint(angle, radius);
		synchronized (panel) {
			panel.setEnd_X_hour((int) point.getX());
			panel.setEnd_Y_hour((int) point.getY());
		}
	}

	private Point getEndPoint(double angle, int radius) {
		Point nullPoint = new Point(panel.getSize().width / 2, panel.getSize().height / 2);
		int x = (int) (nullPoint.getX() - radius * Math.cos(angle));
		int y = (int) (nullPoint.getY() - radius * Math.sin(angle));
		return new Point(x, y);
	}
}
