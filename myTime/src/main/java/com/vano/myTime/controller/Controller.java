package com.vano.myTime.controller;

import javax.swing.JOptionPane;

import com.vano.myTime.model.*;
import com.vano.myTime.view.*;

public class Controller {
    private static volatile Controller instance = null;
    private final UserData userData = new UserData();

    private Controller() {
    }

    public static Controller getInstance() {
        if (instance == null) {
            synchronized (Controller.class) {
                if (instance == null) {
                    instance = new Controller();
                }
            }
        }
        return instance;
    }

    public void init() {
        new TextHandler(userData).initAge();
        new MainFrame(userData).start();
    }

    public void calculateAge(String firstAge, String secondAge, String year, boolean beforeAge) {
        try {
            int yearInt = new TextHandler(userData).validateUserInput(year);
            if (yearInt > 0) {
                new AgeCalculator(userData).calculate(firstAge, secondAge, yearInt, beforeAge);
            } else {
                JOptionPane.showMessageDialog(null, "User set up year <=0");
            }
        } catch (UnsupportedOperationException ex) {
            ex.printStackTrace();
        }
    }

    public void showDetails() {
        int ageInt;
        synchronized (userData) {
            ageInt = userData.getAgeToChrist();
        }
        if (ageInt != 0) {
            new NetHandler(userData).startWiki();
        } else {
            JOptionPane.showMessageDialog(null, "Year of age inputted incorrectly");
        }
    }
}
