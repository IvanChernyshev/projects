package common.equations.fa�tory;

import common.equations.*;
import common.utils.*;

public class QuadraticType1Creator extends EquationCreator {

	public Equation create() {
		int x2 = 0;
		int x1 = RandomGenerator.getRandom(1, 100);
		int a = RandomGenerator.getRandom(-100, -1);
		int b = RandomGenerator.getRandom(-10, -1);
		int c = -b * x1;
		String task = " ������ ���������: \n" + " " + a + "x (" + b + "x + " + c + ") = 0"
				+ "\n � ����� �������� ���������� ������.";
		return new Quadratic(a, b, c, 0, 0, 0, x1, x2, task);
	}
}
