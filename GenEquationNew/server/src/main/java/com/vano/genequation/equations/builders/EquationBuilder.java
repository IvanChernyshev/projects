package com.vano.genequation.equations.builders;

import com.vano.genequation.equations.Equation;

public interface EquationBuilder {
    Equation build();
}
