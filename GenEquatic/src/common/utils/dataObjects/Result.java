package common.utils.dataObjects;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

import common.constants.UtilConstants;
import common.equations.*;
import common.utils.ResumeHandler;

public class Result implements Serializable {

	private static final long serialVersionUID = 1L;
	private final List<Equation> doneEquations;
	private final Person person;
	private final int minutes, seconds, rightTasks, allTasks, rating;
	private final String resume;
	private final String date;

	public Result(List<Equation> doneEquation, Person person, int seconds, int rightTasks, int allTasks) {
		this.doneEquations = Collections.unmodifiableList(doneEquation);
		this.person = person;
		this.minutes = seconds / 60;
		this.seconds = seconds % 60;
		this.rightTasks = rightTasks;
		this.allTasks = allTasks;
		this.rating = (rightTasks * 100) / allTasks;
		this.resume = ResumeHandler.getResume(rating);
		this.date = new SimpleDateFormat(UtilConstants.DATE_FORMAT).format(new Date());
	}

	public List<Equation> getDone() {
		return doneEquations;
	}

	public Person getPerson() {
		return person;
	}

	public int getMin() {
		return minutes;
	}

	public int getSec() {
		return seconds;
	}

	public int getRightTask() {
		return rightTasks;
	}

	public int getAllTask() {
		return allTasks;
	}

	public int getRating() {
		return rating;
	}

	public String getResume() {
		return resume;
	}

	public String getDate() {
		return date;
	}

	@Override
	public String toString() {
		return person.toString() + date;
	}
}
