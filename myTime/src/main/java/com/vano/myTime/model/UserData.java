package com.vano.myTime.model;

import java.util.*;

public class UserData {
    private HashMap<String, Integer> ages;
    private Vector<String> listOfAges;
    private String result;
    private int ageToChrist;
    private String firstAge;

    HashMap<String, Integer> getAges() {
        return ages;
    }

    void setAges(HashMap<String, Integer> ages) {
        this.ages = ages;
    }

    public Vector<String> getListOfAges() {
        return listOfAges;
    }

    void setListOfAges(Vector<String> listOfAges) {
        this.listOfAges = listOfAges;
    }

    public String getResult() {
        return result;
    }

    void setResult(String result) {
        this.result = result;
    }

    public int getAgeToChrist() {
        return ageToChrist;
    }

    void setAgeToChrist(int ageToChrist) {
        this.ageToChrist = ageToChrist;
    }

    String getFirstAge() {
        return firstAge;
    }

    public void setFirstAge(String firstAge) {
        this.firstAge = firstAge;
    }
}
