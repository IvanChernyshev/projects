package student.controller;

import student.main.*;
import common.constants.*;
import common.utils.dataObjects.*;
import student.model.*;

public class DetailController extends AbstractController {

	private static volatile DetailController instance = null;

	private DetailController() {
	}

	public final static DetailController getInstance() {
		if (instance == null) {
			synchronized (DetailController.class) {
				if (instance == null) {
					instance = new DetailController();
				}
			}
		}
		return instance;
	}

	public void saveData(SaveDataObject dataObject) {
		SaveModel saveModel = new SaveModel(UserData.getInstance());
		saveModel.init(dataObject);
		selectPanel(PanelsConstants.RESULT_PANEL);
		clearPanel(PanelsConstants.DETAIL_PANEL);
	}

	public void closePanel() {
		selectPanel(PanelsConstants.RESULT_PANEL);
		clearPanel(PanelsConstants.DETAIL_PANEL);
	}
}
