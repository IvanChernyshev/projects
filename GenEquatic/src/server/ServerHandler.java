package server;

import java.io.*;
import java.net.*;

import common.utils.dataObjects.*;

public class ServerHandler implements Runnable {
	private ObjectInputStream inputStream;
	private ObjectOutputStream outputStream;
	private StartConnection start;
	private Server server;

	public ServerHandler(Socket socket, Server server, StartConnection st) {
		this.server = server;
		this.start = st;
		try {
			inputStream = new ObjectInputStream(socket.getInputStream());
			outputStream = new ObjectOutputStream(socket.getOutputStream());
			Integer version = (Integer) inputStream.readObject();
			if (version.equals(1)) {
				synchronized (server) {
					server.getTeacherStreams().add(outputStream);
				}
			} else {
				synchronized (server) {
					server.getStudentStreams().add(outputStream);
				}
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void run() {
		Object obj = null;
		try {
			try {
				while ((obj = inputStream.readObject()) != null) {
					responce(obj);
				}
			} catch (SocketException se) {
				start.deleteNumberOfConnection();
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public void responce(Object o) {
		if (o instanceof Task) {
			for (ObjectOutputStream obs : server.getStudentStreams()) {
				try {
					obs.writeObject(o);
				} catch (IOException ex) {
					ex.printStackTrace();
				}
			}
		} else {
			if (o instanceof Result)
				for (ObjectOutputStream obs : server.getTeacherStreams()) {
					try {
						obs.writeObject(o);
					} catch (IOException ex) {
						ex.printStackTrace();
					}
				}
		}
	}
}
