package student.main;

import java.util.*;

import student.view.*;

public class SetUpView {
	private final List<AbstractPanel> listOfViews;

	public SetUpView(UserData userData) {
		this.listOfViews = new ArrayList<AbstractPanel>();
		this.listOfViews.add(new JoinPanel(userData));
		this.listOfViews.add(new LoginPanel(userData));
		this.listOfViews.add(new TypeOfWorkPanel(userData));
		this.listOfViews.add(new AnswerPanel(userData));
		this.listOfViews.add(new TaskPanel(userData));
		this.listOfViews.add(new StartTestPanel(userData));
		this.listOfViews.add(new SaveTaskPanel(userData));
		this.listOfViews.add(new ResultPanel(userData));
		this.listOfViews.add(new DetailPanel(userData));
	}

	public List<AbstractPanel> getListOfPanels() {
		return listOfViews;
	}
}
