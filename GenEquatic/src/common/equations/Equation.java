package common.equations;

import java.io.*;

public abstract class Equation implements Comparable<Equation>, Serializable {
	private static final long serialVersionUID = 1L;
	String name;
	String task;
	int a, b, c, d, f, g, h, x1, x2;
	int userReply;
	boolean isDone;
	int number;

	public Equation(int A, int B, int C, int D, int F, int G, int X1, int X2, String task) {
		this.a = A;
		this.b = B;
		this.c = C;
		this.d = D;
		this.f = F;
		this.g = G;
		this.x1 = X1;
		this.x2 = X2;
		this.task = task;
	}

	public String toString() {
		name = " ������� " + number + "\n" + task + "\n" + " ������ ���������:" + "  " + getDone() + "\n"
				+ " ������ �����:" + "  " + x1 + "\n" + " ��������� �����:" + "  " + userReply;
		return name;
	}

	public String getTask() {
		return task;
	}

	public void compareReply() {
		setIsDone(x1 == userReply);
	}

	public int getRightReply() {
		return x1;
	}

	public void setUserReply(int userReply) {
		this.userReply = userReply;
	}

	public void setIsDone(boolean isDone) {
		this.isDone = isDone;
	}

	public String getDone() {
		return isDone ? "��" : "���";
	}

	public boolean getIsDone() {
		return isDone;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public int getNumber() {
		return number;
	}

	public int compareTo(Equation equation) {
		return this.getNumber() - equation.getNumber();

	}
}
