package common.constants;

public class TaskConstants {
	public static final int LINEAR = 0;
	public static final int QUADRATIC = 1;
	public static final int FRACTIONAL = 2;
	public static final int COMBAINED = 3;
	public static final int RANDOM = 4;
	public static final int TIME = 5;
}
