package com.vano.RevelationServer;

import java.io.*;
import java.net.*;
import javax.swing.*;

public class ServerStart implements Runnable {
	private Server server;

	public ServerStart(Server server) {
		this.server = server;
	}

	@Override
	public void run() {
		while (true) {
			try {
				Socket socket = server.getServerSocket().accept();
				server.getSockets().add(socket);
				Thread clientConnection = new Thread(new ServerHandler(socket));
				clientConnection.setDaemon(true);
				clientConnection.start();
			} catch (IOException e) {
				JOptionPane.showMessageDialog(null, "Ошибка при создании объекта Socket");
				e.printStackTrace();
			}
		}
	}
}
