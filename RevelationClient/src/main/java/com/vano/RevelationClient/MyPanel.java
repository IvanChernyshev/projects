package com.vano.RevelationClient;

import java.awt.*;
import javax.swing.*;

public class MyPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	@Override
	public void paintComponent(Graphics g) {
		ImageIcon icon = new ImageIcon(MyPanel.class.getResource("/image/bg.jpeg"));
		g.drawImage(icon.getImage(), 0, 0, null);
	}

}
