package teacher.view;

import javax.swing.*;
import javax.swing.event.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

import teacher.main.*;
import common.utils.*;
import common.utils.dataObjects.Result;
import teacher.controller.*;

public class AnswerPanel extends AbstractPanel {
	private JList<Result> list;
	private Vector<Result> arrayOfResults;

	public AnswerPanel(UserData userData) {
		super(userData);
	}

	@Override
	protected void setStatic() {
		mainPanel = new JPanel();
		mainPanel.setBounds(0, 0, 384, 471);
		mainPanel.setBackground(Color.lightGray);
		mainPanel.setBorder(TextHandler.getBorder2());
		mainPanel.setLayout(null);

		JPanel topPanel = new JPanel();
		topPanel.setBounds(10, 11, 364, 50);
		mainPanel.add(topPanel);
		topPanel.setLayout(null);
		topPanel.setBackground(Color.lightGray);
		topPanel.setBorder(TextHandler.getBorder1());

		JLabel topLabel = new JLabel("�������� �������");
		topLabel.setBounds(87, 11, 217, 28);
		topLabel.setFont(TextHandler.getBigFont());
		topPanel.add(topLabel);
	}

	@Override
	protected void setDynamic() {
		JPanel lowPanel = new JPanel();
		lowPanel.setBounds(10, 72, 364, 327);
		mainPanel.add(lowPanel);
		lowPanel.setLayout(null);
		lowPanel.setBackground(Color.lightGray);
		lowPanel.setBorder(TextHandler.getBorder1());

		list = new JList<Result>();
		list.setBounds(10, 11, 344, 305);
		list.addListSelectionListener(new ListListener());
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		JScrollPane scroll = new JScrollPane(list);
		scroll.setBounds(10, 11, 344, 305);
		scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		scroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		lowPanel.add(scroll);

		JButton mainButton = new JButton("�� �������");
		mainButton.setBounds(240, 410, 135, 50);
		mainPanel.add(mainButton);
		mainButton.setFont(TextHandler.getLittleFont());
		mainButton.addActionListener(new MainButtonListener());

		JButton sendButton = new JButton("���������");
		sendButton.setBounds(10, 410, 135, 50);
		mainPanel.add(sendButton);
		sendButton.setFont(TextHandler.getLittleFont());
		sendButton.addActionListener(new SendButtonListener());
	}

	public class MainButtonListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			AnswerController.getInstance().returnToMainPanel();
		}
	}

	public class SendButtonListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			AnswerController.getInstance().sendTask();
		}
	}

	public class ListListener implements ListSelectionListener {
		@Override
		public void valueChanged(ListSelectionEvent e) {
			if (!e.getValueIsAdjusting()) {
				Result selected = list.getSelectedValue();
				if (selected != null) {
					AnswerController.getInstance().setResult(selected);
				}
			}
		}
	}

	@Override
	public void update(Observable o, Object arg) {
		if (o instanceof UserData) {
			UserData userData = (UserData) o;
			arrayOfResults = userData.getArrayOfResults();
			updateView();
		}
	}

	@Override
	public void updateView() {
		list.setListData(arrayOfResults);
	}
}
