package server;

//192.168.10.106
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.net.*;
import javax.swing.*;
import java.util.*;

public class Server {
	private boolean open;
	private JFrame frame;
	private JLabel lowLabel, ipLabel, connectionLabel;
	private JButton srartButton;
	private ArrayList<ObjectOutputStream> studentStream, teacherStream;
	private ArrayList<Socket> arrayOfSockets;
	private ServerSocket serverSocket;
	private InetAddress adr;
	private String ip;
	private int numberOfConnection;

	public static void main(String[] args) {
		Server server = new Server();
		server.go();
	}

	public void go() {
		studentStream = new ArrayList<ObjectOutputStream>();
		teacherStream = new ArrayList<ObjectOutputStream>();
		arrayOfSockets = new ArrayList<Socket>();
		setUpGui();
	}

	public void setUpGui() {
		frame = new JFrame("Server");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		frame.setBounds(500, 200, 300, 200);
		frame.getContentPane().setLayout(null);

		MyPanel panel = new MyPanel();
		panel.setBounds(0, 0, 294, 171);
		frame.getContentPane().add(panel);
		panel.setLayout(null);

		srartButton = new JButton("���������� ����������");
		srartButton.setBounds(64, 48, 174, 43);
		panel.add(srartButton);
		srartButton.addActionListener(new StartListener());

		lowLabel = new JLabel("���������� �������");
		lowLabel.setBounds(0, 0, 294, 29);
		lowLabel.setFont(new Font("TimesRoman", Font.BOLD, 18));
		lowLabel.setHorizontalAlignment(JLabel.CENTER);
		lowLabel.setForeground(Color.RED);
		panel.add(lowLabel);

		connectionLabel = new JLabel();
		connectionLabel.setBounds(0, 102, 294, 29);
		panel.add(connectionLabel);
		connectionLabel.setVisible(false);
		connectionLabel.setFont(new Font("TimesRoman", Font.BOLD, 18));
		connectionLabel.setHorizontalAlignment(JLabel.CENTER);
		connectionLabel.setForeground(Color.GREEN);

		ipLabel = new JLabel();
		ipLabel.setBounds(0, 142, 294, 29);
		panel.add(ipLabel);
		ipLabel.setFont(new Font("TimesRoman", Font.BOLD, 18));
		ipLabel.setHorizontalAlignment(JLabel.CENTER);
		ipLabel.setForeground(Color.GREEN);

		ipLabel.setVisible(false);
		frame.setResizable(false);
		frame.revalidate();
		frame.repaint();
	}

	public class StartListener implements ActionListener {
		public void actionPerformed(ActionEvent ev) {
			if (!open) {
				try {
					adr = InetAddress.getLocalHost();
					ip = adr.getHostAddress();
				} catch (UnknownHostException ex) {
					ex.printStackTrace();
				}
				lowLabel.setText("���������� �����������");
				lowLabel.setForeground(Color.GREEN);
				srartButton.setText("������� ����������");
				connectionLabel.setText("������������� ����������: " + numberOfConnection);
				connectionLabel.setVisible(true);
				ipLabel.setText("IPv4: " + ip);
				ipLabel.setVisible(true);
				open = true;
				setUpConection();
			} else {
				endConection();
				lowLabel.setText("���������� �������");
				lowLabel.setForeground(Color.RED);
				srartButton.setText("���������� ����������");
				ipLabel.setVisible(false);
				connectionLabel.setVisible(false);
				open = false;
			}
		}
	}

	public void setUpConection() {
		try {
			serverSocket = new ServerSocket(3333);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		Thread startConnection = new Thread(new StartConnection(this));
		startConnection.start();
	}

	public void endConection() {
		try {
			serverSocket.close();
			for (Socket socket : arrayOfSockets) {
				socket.close();
			}
		} catch (BindException ex) {
			ex.printStackTrace();
		} catch (SocketException ex) {
			ex.printStackTrace();
		} catch (IOException iex) {
			iex.printStackTrace();
		}
		studentStream.clear();
		teacherStream.clear();
		arrayOfSockets.clear();
		adr = null;
		ip = null;
	}

	public ArrayList<ObjectOutputStream> getStudentStreams() {
		return studentStream;
	}

	public ArrayList<ObjectOutputStream> getTeacherStreams() {
		return teacherStream;
	}

	public ArrayList<Socket> getArrayOfSockets() {
		return arrayOfSockets;
	}

	public ServerSocket getServerSocket() {
		return serverSocket;
	}

	public JLabel getConnectionLabel() {
		return connectionLabel;
	}
}
