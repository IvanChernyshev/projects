package web;

import domain.Journal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import repository.JournalRepository;

import java.util.List;

@Controller
public class JournalController {

    @Autowired
    private JournalRepository repository;

    @RequestMapping("/")
    public String index(Model model) {
        model.addAttribute("journal", repository.findAll());
        return "index";
    }

    @RequestMapping(value = "/journal", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    public @ResponseBody
    List<Journal> getJournal() {
        return repository.findAll();
    }
}
