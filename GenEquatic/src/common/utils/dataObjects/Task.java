package common.utils.dataObjects;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

import common.constants.UtilConstants;
import common.equations.*;

public class Task implements Serializable {

	private static final long serialVersionUID = 1L;
	private final List<Equation> taskEquations;
	private final int time;
	private final Person person;
	private final String date;

	public Task(List<Equation> taskArray, int time, Person person) {
		this.taskEquations = taskArray;
		this.time = time;
		this.person = person;
		this.date = new SimpleDateFormat(UtilConstants.DATE_FORMAT).format(new Date());
	}

	public List<Equation> getTaskEquations() {
		return taskEquations;
	}

	public int getTime() {
		return time;
	}

	public Person getPerson() {
		return person;
	}

	public String getDate() {
		return date;
	}

	@Override
	public String toString() {
		return person.toString() + date;
	}
}
