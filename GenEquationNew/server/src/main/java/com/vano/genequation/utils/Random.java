package com.vano.genequation.utils;

public class Random {

    public static int getRandomInt(int min, int max) {
        return min + (int) (Math.random() * (max - min) + 1);
    }
}
