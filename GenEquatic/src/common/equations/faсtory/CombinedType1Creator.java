package common.equations.fa�tory;

import common.equations.*;
import common.utils.*;

public class CombinedType1Creator extends EquationCreator {

	public Equation create() {
		int x1 = RandomGenerator.getRandom(1, 11);
		int a = RandomGenerator.getRandom(2, 7);
		int c = RandomGenerator.getRandom(8, 13);
		int b = -1 * (c * c + a * a + 2 * a * x1);
		String task = " ������ ���������: \n" + " " + "(x + " + a + ")^2 " + b + " = (x - " + c + ")(x + " + c + ")";
		return new Combined(a, b, c, 0, 0, 0, x1, 0, task);
	}
}
