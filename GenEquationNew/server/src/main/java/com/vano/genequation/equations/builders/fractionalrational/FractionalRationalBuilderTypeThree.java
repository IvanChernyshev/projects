package com.vano.genequation.equations.builders.fractionalrational;

import com.vano.genequation.equations.Equation;
import com.vano.genequation.utils.Const;

import java.util.UUID;

import static com.vano.genequation.utils.Const.SOLVE_EQUATION_EN;
import static com.vano.genequation.utils.Random.getRandomInt;

public class FractionalRationalBuilderTypeThree extends FractionalRationalBuilder {

    @Override
    public Equation build() {
        final int reply = getRandomInt(1, 11);
        final int a = getRandomInt(2, 7);
        final int c = getRandomInt(8, 13);
        final int b = reply * (c - a);
        return Equation.builder()
                .id(UUID.randomUUID())
                .equationType(Const.EquationType.FRACTIONAL_RATIONAL)
                .description(SOLVE_EQUATION_EN)
                .body("(x^2 + " + a + "x + " + b + ")/(x + " + c + ") = x")
                .rightReply(reply)
                .build();
    }
}
