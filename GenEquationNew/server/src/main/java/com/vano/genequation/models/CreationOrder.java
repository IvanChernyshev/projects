package com.vano.genequation.models;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CreationOrder {
    private int linearCount;
    private int quadraticCount;
    private int fractionalCount;
    private int combinedCount;
    private int randomCount;
}
