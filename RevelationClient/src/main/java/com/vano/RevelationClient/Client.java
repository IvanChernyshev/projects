package com.vano.RevelationClient;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.net.*;
import javax.swing.*;

public class Client {
	private MyPanel panel;
	private Socket socket;
	private JTextArea textArea;
	private JLabel verseLabel;

	public static void main(String[] args) {
		new Client().setUpGui();
	}

	public void setUpGui() {
		JFrame frame = new JFrame("Откровения Иоанна Богослова");
		frame.setBounds(300, 100, 450, 300);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.getContentPane().setLayout(new BorderLayout(0, 0));

		panel = new MyPanel();
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(null);

		JButton verseButton = new JButton("Получить жизнеутверждающую цитату");
		verseButton.setBounds(10, 11, 424, 45);
		verseButton.setBackground(Color.gray);
		verseButton.setForeground(Color.green);
		verseButton.setFont(new Font("Verdana", Font.ITALIC, 17));
		verseButton.addActionListener(new VerseListener());
		panel.add(verseButton);

		textArea = new JTextArea("");
		textArea.setBounds(10, 129, 424, 131);
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		textArea.setFont(new Font("Verdana", Font.ITALIC, 17));
		textArea.setForeground(Color.red);
		textArea.setEditable(false);
		textArea.setBackground(Color.lightGray);
		JScrollPane scroll = new JScrollPane(textArea);
		scroll.setBounds(10, 129, 424, 131);
		scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		scroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		panel.add(scroll);

		verseLabel = new JLabel("Стих: Строка:");
		verseLabel.setBounds(10, 66, 424, 52);
		verseLabel.setForeground(Color.green);
		verseLabel.setFont(new Font("Verdana", Font.ITALIC, 30));
		verseLabel.setHorizontalAlignment(JLabel.CENTER);
		panel.add(verseLabel);

		frame.repaint();
		frame.revalidate();
	}

	public class VerseListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			findVerse();
		}
	}

	public void findVerse() {
		try {
			socket = new Socket("127.0.0.1", 4445);
			Thread netHandler = new Thread(new NetHandler(socket));
			netHandler.setDaemon(true);
			netHandler.start();
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Ошибка в работе объекта Socket");
			e.printStackTrace();
		}
	}

	public class NetHandler implements Runnable {
		Socket socket;

		public NetHandler(Socket socket) {
			this.socket = socket;
		}

		@Override
		public void run() {
			try (ObjectInputStream inputstream = new ObjectInputStream(socket.getInputStream());) {
				try {
					String string = (String) inputstream.readObject();
					String chapter = (String) inputstream.readObject();
					textArea.setText(string);
					verseLabel.setText(chapter);
					panel.repaint();
				} catch (ClassNotFoundException e) {
					JOptionPane.showMessageDialog(null, "Ошибка в преобразовании объекта String");
					e.printStackTrace();
				}
			} catch (IOException e) {
				JOptionPane.showMessageDialog(null, "Ошибка в работе объекта ввода");
				e.printStackTrace();
			} finally {
				try {
					socket.close();
				} catch (IOException e) {
					JOptionPane.showMessageDialog(null, "Ошибка при закрытии объекта Socket");
					e.printStackTrace();
				}
			}
		}
	}
}
