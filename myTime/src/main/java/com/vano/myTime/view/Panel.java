package com.vano.myTime.view;

import java.awt.Graphics;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

public class Panel extends JPanel {

    private static final long serialVersionUID = 1L;

    @Override
    public void paintComponent(Graphics g) {
        ImageIcon icon = new ImageIcon(Panel.class.getResource("/image/bg.jpeg"));
        g.drawImage(icon.getImage(), 0, 0, null);
    }
}
