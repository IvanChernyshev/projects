package com.vano.genequation.equations.builders.quadratic;

import com.vano.genequation.equations.Equation;
import com.vano.genequation.utils.Const;

import java.util.UUID;

import static com.vano.genequation.utils.Const.SOLVE_EQUATION_WITH_MAJOR_ROOT_EN;
import static com.vano.genequation.utils.Random.getRandomInt;

public class QuadraticBuilderTypeOne extends QuadraticBuilder {

    @Override
    public Equation build() {
        final int reply = getRandomInt(1, 100);
        final int a = getRandomInt(-100, -1);
        final int b = getRandomInt(-10, -1);
        final int c = -b * reply;
        return Equation.builder()
                .id(UUID.randomUUID())
                .equationType(Const.EquationType.QUADRATIC)
                .description(SOLVE_EQUATION_WITH_MAJOR_ROOT_EN)
                .body(a + "x (" + b + "x + " + c + ") = 0")
                .rightReply(reply)
                .build();
    }
}
