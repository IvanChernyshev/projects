package common.equations.fa�tory;

import common.equations.*;
import common.utils.*;

public class LinearType2Creator extends EquationCreator {

	public Equation create() {
		int x1 = RandomGenerator.getRandom(-100, 100);
		int a = RandomGenerator.getRandom(-10, -1);
		int b = RandomGenerator.getRandom(1, 100);
		int c = x1 * a + b;
		String task = " ������ ���������: \n" + " " + a + "x + " + b + " = " + c;
		return new Linear(a, b, c, 0, 0, 0, x1, 0, task);
	}
}
