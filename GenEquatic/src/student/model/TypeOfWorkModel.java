package student.model;

import student.main.*;

public class TypeOfWorkModel extends AbstractModel {

	public TypeOfWorkModel(UserData userData) {
		super(userData);
	}

	public void setTypeOfWork(boolean selfWorking) {
		userData.setSelfWorking(selfWorking);
	}

}
