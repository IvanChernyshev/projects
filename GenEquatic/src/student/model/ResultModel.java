package student.model;

import java.util.*;

import student.controller.*;
import student.main.*;
import common.equations.*;
import common.utils.dataObjects.*;

public class ResultModel extends AbstractModel {

	public ResultModel(UserData userData) {
		super(userData);
	}

	public void setResult(ResultDataObject dataObject) {
		int seconds;
		List<Equation> listOfDoneEquations = dataObject.getListOfDoneEquations();
		Collections.sort(listOfDoneEquations);
		Person person = userData.getPerson();
		synchronized (TestData.getInstance()) {
			seconds = TestData.getInstance().getSecondsDone();
		}
		int rightTasks = setRightTasks(listOfDoneEquations);
		int allTasks = dataObject.getNumberOfTasks();
		sendResult(listOfDoneEquations, person, seconds, rightTasks, allTasks);
	}

	private int setRightTasks(List<Equation> listOfDoneEquations) {
		int rightTasks = 0;
		for (Equation equation : listOfDoneEquations) {
			equation.compareReply();
			if (equation.getIsDone()) {
				rightTasks++;
			}
		}
		return rightTasks;
	}

	private void sendResult(List<Equation> listOfDoneEquations, Person person, int seconds, int rightTasks,
			int allTasks) {
		Result result = new Result(listOfDoneEquations, person, seconds, rightTasks, allTasks);
		synchronized (UserData.getInstance()) {
			userData.setResult(result);
		}
		if (!UserData.getInstance().isSelfWorking()) {
			NetController.getInstance().sendMessage();
		}
	}
}
