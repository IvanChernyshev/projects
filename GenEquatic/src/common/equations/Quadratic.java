package common.equations;

public class Quadratic extends Equation {

	private static final long serialVersionUID = 1L;

	public Quadratic(int A, int B, int C, int D, int F, int G, int X1, int X2, String T) {
		super(A, B, C, D, F, G, X1, X2, T);
	}
}
