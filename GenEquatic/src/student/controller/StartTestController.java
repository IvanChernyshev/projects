package student.controller;

import java.util.logging.Level;
import javax.swing.*;

import student.main.*;
import common.constants.*;
import common.utils.dataObjects.*;
import student.model.*;

public class StartTestController extends AbstractController {

	private static volatile StartTestController instance = null;
	private TestModel model;

	private StartTestController() {
		this.model = new TestModel(UserData.getInstance());
	}

	public final static StartTestController getInstance() {
		if (instance == null) {
			synchronized (StartTestController.class) {
				if (instance == null) {
					instance = new StartTestController();
				}
			}
		}
		return instance;
	}

	public void testStart(String userResponse) {
		try {
			validateResponse(userResponse);
			panels.get(PanelsConstants.START_TEST_PANEL).clear();
			frame.repaint();
			if (TestData.getInstance().isNoMoreEquations()) {
				panels.get(PanelsConstants.START_TEST_PANEL).end();
			}
		} catch (UnsupportedOperationException ex) {
			Log.log(Level.SEVERE, "Not resolved user input text in StartTestPanel", ex);
		}
	}

	public void testStop() {
		panels.get(PanelsConstants.START_TEST_PANEL).end();
		model.testStop();
		panels.get(PanelsConstants.START_TEST_PANEL).clearAll();
		selectPanel(PanelsConstants.RESULT_PANEL);
	}

	private void validateResponse(String userResponse) {
		if (userResponse.equals("")) {
			model.nextStep();
		} else {
			validateForm(userResponse);
		}
		updateDataOfView();
	}

	private void validateForm(String userResponse) {
		try {
			int userResponseInt = Integer.parseInt(userResponse);
			model.doStepWithReply(userResponseInt);
		} catch (NumberFormatException ex) {
			Log.log(Level.SEVERE, "User inputed no numbers", ex);
			JOptionPane.showMessageDialog(null, "� ���� ����� ����� �������� ��������� ��������!");
			panels.get(PanelsConstants.START_TEST_PANEL).clear();
			throw new UnsupportedOperationException();
		}
	}

	private void updateDataOfView() {
		panels.get(PanelsConstants.START_TEST_PANEL).updateView();
	}

	public void initModel() {
		model.init();
	}
}
