package teacher.controller;

public class CommonController extends AbstractController {
	private static volatile CommonController instance = null;

	private CommonController() {
	}

	public final static CommonController getInstance() {
		if (instance == null) {
			synchronized (CommonController.class) {
				if (instance == null) {
					instance = new CommonController();
				}
			}
		}
		return instance;
	}
}
