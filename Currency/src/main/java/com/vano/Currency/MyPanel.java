package com.vano.Currency;

import java.awt.*;
import javax.swing.*;

public class MyPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	public MyPanel() {
		JLabel fromLabel = new JLabel("Today one");
		fromLabel.setBounds(10, 11, 424, 38);
		fromLabel.setForeground(Color.green);
		fromLabel.setFont(new Font("Verdana", Font.ITALIC, 25));
		fromLabel.setHorizontalAlignment(JLabel.CENTER);
		fromLabel.setBackground(Color.gray);
		this.add(fromLabel);
	}

	@Override
	public void paintComponent(Graphics g) {
		ImageIcon icon = new ImageIcon(MyPanel.class.getResource("/image/bg.jpeg"));
		g.drawImage(icon.getImage(), 0, 0, null);
	}

}
