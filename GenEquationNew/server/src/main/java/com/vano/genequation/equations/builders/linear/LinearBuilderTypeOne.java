package com.vano.genequation.equations.builders.linear;

import com.vano.genequation.equations.Equation;
import com.vano.genequation.utils.Const;

import java.util.UUID;

import static com.vano.genequation.utils.Const.SOLVE_EQUATION_EN;
import static com.vano.genequation.utils.Random.getRandomInt;

public class LinearBuilderTypeOne extends LinearBuilder {

    @Override
    public Equation build() {
        final int reply = getRandomInt(-15, 15);
        final int a = getRandomInt(-10, -1);
        final int b = getRandomInt(1, 10);
        final int c = getRandomInt(-10, -1);
        final int d = getRandomInt(1, 10);
        final int f = reply * a - b * (c + d * reply);
        return Equation.builder()
                .id(UUID.randomUUID())
                .equationType(Const.EquationType.LINEAR)
                .description(SOLVE_EQUATION_EN)
                .body(a + "x - " + b + "(" + c + " + " + d + "x" + ") = " + f)
                .rightReply(reply)
                .build();
    }
}
