package teacher.view;

import javax.swing.*;

public class MainFrame {
	private final JFrame frame;
	private JPanel currentPanel;

	public MainFrame(String nameOfVersion) {
		frame = new JFrame(nameOfVersion);
		frame.setBounds(500, 150, 390, 500);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
	}

	public void repaint() {
		frame.revalidate();
		frame.repaint();
	}

	public JFrame getFrame() {
		return frame;
	}

	public void setPanel(AbstractPanel panel) {
		currentPanel = panel.getPanel();
		frame.getContentPane().add(currentPanel);
	}

	public void deletePanel() {
		frame.remove(currentPanel);
	}
}
