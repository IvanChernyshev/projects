package common.equations.fa�tory;

import common.equations.*;

public abstract class EquationCreator {

	public abstract Equation create();
}
