package com.vano.myTime.model;

import com.vano.myTime.view.SecondaryFrame;

public class NetHandler {
    private final UserData userData;

    public NetHandler(UserData userData) {
        this.userData = userData;
    }

    public void startWiki() {
        int age;
        String ageString;
        synchronized (userData) {
            age = userData.getAgeToChrist();
        }
        if (age > 0) {
            ageString = age + "_AD";
        } else {
            ageString = Math.abs(age) + "_BC";
        }
        try {
            new WikiParser().parseResponse(ageString);
        } catch (UnsupportedOperationException ex) {
            noConnection();
        }
    }

    private void noConnection() {
        String age;
        synchronized (userData) {
            age = userData.getFirstAge();
        }
        String result = new TextHandler(userData).localDescription(age);
        if (!result.equals("")) {
            new SecondaryFrame(result).start();
        }
    }
}
