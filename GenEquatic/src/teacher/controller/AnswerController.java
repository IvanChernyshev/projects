package teacher.controller;

import teacher.main.*;
import common.constants.*;
import common.utils.dataObjects.*;
import teacher.model.*;

public class AnswerController extends AbstractController {

	private static volatile AnswerController instance = null;

	private AnswerController() {
	}

	public final static AnswerController getInstance() {
		if (instance == null) {
			synchronized (AnswerController.class) {
				if (instance == null) {
					instance = new AnswerController();
				}
			}
		}
		return instance;
	}

	public void setResult(Result result) {
		AnswerModel model = new AnswerModel(UserData.getInstance());
		model.setResult(result);
		selectPanel(PanelsConstants.RESULT_PANEL);
	}

	public void returnToMainPanel() {
		selectPanel(PanelsConstants.TYPE_OF_WORK_PANEL);
	}

	public void sendTask() {
		selectPanel(PanelsConstants.TASK_PANEL);
	}
}
