package teacher.model;

import teacher.main.*;
import common.utils.dataObjects.*;

public class AnswerModel extends AbstractModel {

	public AnswerModel(UserData userData) {
		super(userData);
	}

	public void setResult(Result result) {
		synchronized (UserData.getInstance()) {
			userData.setResult(result);
		}
	}
}
