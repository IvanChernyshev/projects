package com.vano.RevelationServer;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.net.*;
import java.util.*;
import javax.swing.*;

public class Server {
	private JButton startButton, closeButton;
	private ServerSocket serverSocket;
	private ArrayList<Socket> sockets;

	public static void main(String[] args) {
		Server server = new Server();
		server.setUpGui();
	}

	public Server() {
		sockets = new ArrayList<Socket>();
		try {
			serverSocket = new ServerSocket(4445);
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Ошибка в работе объекта ServerSocket");
			e.printStackTrace();
			System.exit(0);
		}
	}

	public ServerSocket getServerSocket() {
		return serverSocket;
	}

	public ArrayList<Socket> getSockets() {
		return sockets;
	}

	private void setUpGui() {
		JWindow window = new JWindow();
		window.setBounds(500, 200, 200, 200);
		window.getContentPane().setLayout(new BorderLayout(0, 0));

		MyPanel panel = new MyPanel();
		window.getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(null);

		startButton = new JButton("Start server");
		startButton.setBounds(10, 86, 180, 46);
		startButton.setBackground(Color.gray);
		startButton.setForeground(Color.green);
		startButton.setFont(new Font("Verdana", Font.ITALIC, 20));
		startButton.addActionListener(new StartListener());
		panel.add(startButton);

		closeButton = new JButton("Close server");
		closeButton.setBounds(10, 143, 180, 46);
		closeButton.setBackground(Color.gray);
		closeButton.setForeground(Color.red);
		closeButton.setFont(new Font("Verdana", Font.ITALIC, 20));
		closeButton.addActionListener(new CloseListener());
		panel.add(closeButton);

		JLabel serverLabel = new JLabel("SimpleServer");
		serverLabel.setBounds(10, 11, 180, 33);
		serverLabel.setHorizontalAlignment(JLabel.CENTER);
		serverLabel.setForeground(Color.green);
		serverLabel.setFont(new Font("Verdana", Font.ITALIC, 20));
		panel.add(serverLabel);
		serverOff();
		window.setVisible(true);
	}

	public class StartListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			serverOn();
			start();
		}
	}

	public class CloseListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			serverOff();
			end();
			System.exit(0);
		}
	}

	private void serverOn() {
		startButton.setEnabled(false);
		closeButton.setEnabled(true);
	}

	private void serverOff() {
		startButton.setEnabled(true);
		closeButton.setEnabled(false);
	}

	private void start() {
		Thread connection = new Thread(new ServerStart(this));
		connection.setDaemon(true);
		connection.start();
	}

	private void end() {
		try {
			serverSocket.close();
			for (Socket socket : sockets) {
				socket.close();
			}
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Ошибка в работе объекта ServerSocket");
			e.printStackTrace();
		}
	}
}
