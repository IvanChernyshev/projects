package com.vano.genequation.equations.builders.quadratic;

import com.vano.genequation.equations.Equation;
import com.vano.genequation.utils.Const;

import java.util.UUID;

import static com.vano.genequation.utils.Const.SOLVE_EQUATION_WITH_MINOR_ROOT_EN;
import static com.vano.genequation.utils.Random.getRandomInt;

public class QuadraticBuilderTypeThree extends QuadraticBuilder {

    @Override
    public Equation build() {
        final int reply_1 = getRandomInt(1, 8);
        final int reply_2 = getRandomInt(9, 15);
        final int b = -(reply_1 + reply_2);
        final int c = (reply_1 * reply_2);
        return Equation.builder()
                .id(UUID.randomUUID())
                .equationType(Const.EquationType.QUADRATIC)
                .description(SOLVE_EQUATION_WITH_MINOR_ROOT_EN)
                .body("x^2 " + b + "x + " + c + " = 0")
                .rightReply(reply_1)
                .build();
    }
}
