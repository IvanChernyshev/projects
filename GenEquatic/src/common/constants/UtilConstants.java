package common.constants;

public final class UtilConstants {
	public final static String SEPARATOR = "------------------------------------------------------";
	public final static String DEFAULT_IP_ADDRESS = "127.0.0.1";
	public final static int PORT = 3333;
	public final static String DATE_FORMAT = "dd.MM.yyyy HH:mm";
}
