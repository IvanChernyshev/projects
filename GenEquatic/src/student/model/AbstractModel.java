package student.model;

import java.util.logging.*;

import student.main.*;

public abstract class AbstractModel {
	protected UserData userData;
	protected Logger Log;

	public AbstractModel(UserData userData) {
		this.userData = userData;
		Log = Logger.getLogger(this.getClass().getName());
	}
}
