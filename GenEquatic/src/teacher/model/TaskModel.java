package teacher.model;

import java.util.*;

import teacher.main.*;
import common.constants.*;
import common.equations.*;
import common.utils.dataObjects.*;

public class TaskModel extends AbstractModel {

	public TaskModel(UserData userData) {
		super(userData);
	}

	public void setTask(List<Integer> checkedUserInput) {
		int time = checkedUserInput.get(TaskConstants.TIME);
		Person person = userData.getPerson();
		List<Equation> listOfEquations = generateEquations(checkedUserInput);
		Task task = new Task(listOfEquations, time, person);
		synchronized (UserData.getInstance()) {
			userData.setTask(task);
		}
	}

	private List<Equation> generateEquations(List<Integer> numberOfEquations) {
		int linear = numberOfEquations.get(TaskConstants.LINEAR);
		int quadratic = numberOfEquations.get(TaskConstants.QUADRATIC);
		int fractional = numberOfEquations.get(TaskConstants.FRACTIONAL);
		int combained = numberOfEquations.get(TaskConstants.COMBAINED);
		int random = numberOfEquations.get(TaskConstants.RANDOM);
		SetUpEquations setUp = new SetUpEquations();
		setUp.setUp(linear, quadratic, fractional, combained, random);
		List<Equation> arrayOfEquation = setUp.getList();
		int numOfEq = 1;
		for (Equation equation : arrayOfEquation) {
			equation.setNumber(numOfEq);
			numOfEq++;
		}
		return arrayOfEquation;
	}
}
