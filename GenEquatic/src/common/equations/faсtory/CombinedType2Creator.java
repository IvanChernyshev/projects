package common.equations.fa�tory;

import common.equations.*;
import common.utils.*;

public class CombinedType2Creator extends EquationCreator {

	public Equation create() {
		int x1 = RandomGenerator.getRandom(-10, -1);
		int a = RandomGenerator.getRandom(2, 7);
		int c = RandomGenerator.getRandom(2, 8);
		int b = c * c - x1;
		int d = a * a;
		String task = " ������ ���������: \n" + " " + d + "x^2 - (" + a + "x + " + c + ")(" + a + "x - " + c
				+ ") = x + " + b;
		return new Combined(a, b, c, d, 0, 0, x1, 0, task);
	}
}
