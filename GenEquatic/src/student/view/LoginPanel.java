package student.view;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

import student.main.*;
import common.utils.*;
import common.utils.dataObjects.LoginDataObject;
import student.controller.*;

public class LoginPanel extends AbstractPanel {

	private JTextField firstNameField, secondNameField, groupField, addressField;

	public LoginPanel(UserData userData) {
		super(userData);
	}

	@Override
	protected void setStatic() {
		mainPanel = new JPanel();
		mainPanel.setLayout(null);
		mainPanel.setBackground(Color.lightGray);
		mainPanel.setBorder(TextHandler.getBorder2());

		JPanel choosePanel = new JPanel();
		choosePanel.setBounds(10, 11, 364, 385);
		mainPanel.add(choosePanel);
		choosePanel.setLayout(null);
		choosePanel.setBackground(Color.lightGray);
		choosePanel.setBorder(TextHandler.getBorder1());

		JLabel firstNameLabel = new JLabel("���");
		firstNameLabel.setBounds(10, 11, 120, 32);
		firstNameLabel.setFont(TextHandler.getBigFont());
		choosePanel.add(firstNameLabel);

		JLabel seconNameLabel = new JLabel("�������");
		seconNameLabel.setBounds(10, 99, 120, 32);
		seconNameLabel.setFont(TextHandler.getBigFont());
		choosePanel.add(seconNameLabel);

		JLabel groupLabel = new JLabel("������");
		groupLabel.setBounds(10, 185, 120, 32);
		groupLabel.setFont(TextHandler.getBigFont());
		choosePanel.add(groupLabel);

		JLabel adressLabel = new JLabel("IP address");
		adressLabel.setBounds(10, 271, 120, 32);
		adressLabel.setFont(TextHandler.getBigFont());
		choosePanel.add(adressLabel);

		firstNameField = new JTextField();
		firstNameField.setBounds(10, 61, 344, 32);
		choosePanel.add(firstNameField);
		firstNameField.setColumns(10);
		firstNameField.setFont(TextHandler.getMediumFont());

		secondNameField = new JTextField();
		secondNameField.setBounds(10, 142, 344, 32);
		choosePanel.add(secondNameField);
		secondNameField.setColumns(10);
		secondNameField.setFont(TextHandler.getMediumFont());

		groupField = new JTextField();
		groupField.setBounds(10, 228, 344, 32);
		choosePanel.add(groupField);
		groupField.setColumns(10);
		groupField.setFont(TextHandler.getMediumFont());

		addressField = new JTextField();
		addressField.setBounds(10, 314, 344, 32);
		choosePanel.add(addressField);
		addressField.setColumns(10);
		addressField.setFont(TextHandler.getMediumFont());
	}

	@Override
	protected void setDynamic() {
		JButton okButton = new JButton("�����");
		okButton.setBounds(132, 407, 117, 43);
		mainPanel.add(okButton);
		okButton.setFont(TextHandler.getMediumFont());
		okButton.addActionListener(new OkActionListener());
	}

	public class OkActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent event) {
			LoginDataObject dataObject = new LoginDataObject(firstNameField.getText(), secondNameField.getText(),
					groupField.getText(), addressField.getText());
			LoginController.getInstance().startWorking(dataObject);
		}
	}

	@Override
	public void clearAll() {
		firstNameField.setText("");
		secondNameField.setText("");
		groupField.setText("");
		addressField.setText("");
	}
}
