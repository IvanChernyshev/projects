package student.view;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

import student.main.*;
import common.constants.*;
import common.equations.*;
import common.utils.*;
import common.utils.dataObjects.SaveDataObject;
import common.utils.dataObjects.Task;
import student.controller.*;

public class SaveTaskPanel extends AbstractPanel {
	private JTextArea textArea;
	private Task task;

	public SaveTaskPanel(UserData userData) {
		super(userData);
	}

	@Override
	protected void setStatic() {
		mainPanel = new JPanel();
		mainPanel.setBackground(Color.lightGray);
		mainPanel.setBorder(TextHandler.getBorder2());
		mainPanel.setLayout(null);

		JPanel areaPanel = new JPanel();
		areaPanel.setBounds(10, 96, 364, 354);
		mainPanel.add(areaPanel);
		areaPanel.setBorder(TextHandler.getBorder1());
		areaPanel.setLayout(null);
		areaPanel.setBackground(Color.lightGray);

		textArea = new JTextArea();
		textArea.setBounds(10, 11, 344, 332);
		textArea.setFont(TextHandler.getMediumFont());
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		textArea.setEditable(false);

		JScrollPane scroll = new JScrollPane(textArea);
		scroll.setBounds(10, 11, 344, 332);
		scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		scroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		areaPanel.add(scroll);
	}

	@Override
	protected void setDynamic() {
		JButton saveButton = new JButton("���������");
		saveButton.setBounds(10, 20, 110, 50);
		saveButton.setFont(TextHandler.getLittleFont());
		mainPanel.add(saveButton);
		saveButton.addActionListener(new SaveButtonActionListener());

		JButton mainButton = new JButton("�������");
		mainButton.setBounds(135, 20, 110, 50);
		mainButton.setFont(TextHandler.getLittleFont());
		mainPanel.add(mainButton);
		mainButton.addActionListener(new MainButtonActionListener());

		JButton startButton = new JButton();
		startButton.setText("���������");
		startButton.setBounds(260, 20, 115, 50);
		startButton.setFont(TextHandler.getLittleFont());
		mainPanel.add(startButton);
		startButton.addActionListener(new StartButtonActionListener());
	}

	public class SaveButtonActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			SaveDataObject dataObject = new SaveDataObject(textArea.getText(), mainPanel);
			SaveController.getInstance().saveData(dataObject);
		}
	}

	public class StartButtonActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			SaveController.getInstance().selfWorking();
		}
	}

	public class MainButtonActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			SaveController.getInstance().returnToMainPanel();
		}
	}

	@Override
	public void update(Observable o, Object arg) {
		if (o instanceof UserData) {
			UserData userData = (UserData) o;
			this.task = userData.getTask();
		}
	}

	@Override
	public void clearAll() {
		this.task = null;
		textArea.setText("");
	}

	@Override
	public void updateView() {
		textArea.append(task.getDate() + "\n" + task.getPerson().getFirstName() + " " + task.getPerson().getSecondName()
				+ " " + task.getPerson().getGroup() + "\n" + UtilConstants.SEPARATOR + "\n");
		for (Equation equation : task.getTaskEquations()) {
			textArea.append(" ������� " + equation.getNumber() + "\n" + equation.getTask() + "\n"
					+ UtilConstants.SEPARATOR + "\n");
		}
	}
}
