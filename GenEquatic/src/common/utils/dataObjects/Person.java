package common.utils.dataObjects;

import java.io.*;

public class Person implements Serializable {
	private static final long serialVersionUID = 1L;
	private final String firstName;
	private final String secondName;
	private final String group;
	private final String address;

	public Person(String first, String second, String group, String address) {
		this.firstName = first;
		this.secondName = second;
		this.group = group;
		this.address = address;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getSecondName() {
		return secondName;
	}

	public String getGroup() {
		return group;
	}

	public String getAddress() {
		return address;
	}

	@Override
	public String toString() {
		String description = firstName + " " + secondName + " " + group;
		return description;
	}
}
