package student.main;

import java.util.*;

import common.utils.dataObjects.*;

public class UserData extends Observable {
	private static volatile UserData instance = null;
	private Person person;
	private Task task;
	private Result result;
	private Vector<Task> arrayOfTasks;
	private boolean selfWorking, netWorking;
	private final Integer version = 2;

	private UserData() {
		arrayOfTasks = new Vector<Task>();
	}

	public final static UserData getInstance() {
		if (instance == null) {
			synchronized (UserData.class) {
				if (instance == null) {
					instance = new UserData();
				}
			}
		}
		return instance;
	}

	public void dataChange() {
		setChanged();
		notifyObservers();
	}

	public void setPerson(Person person) {
		this.person = person;
		dataChange();
	}

	public Person getPerson() {
		return person;
	}

	public void setTask(Task task) {
		this.task = task;
		dataChange();
	}

	public Task getTask() {
		return task;
	}

	public void setResult(Result result) {
		this.result = result;
		dataChange();
	}

	public Result getResult() {
		return result;
	}

	public void addToArrayOfTasks(Task task) {
		arrayOfTasks.addElement(task);
		dataChange();
	}

	public Vector<Task> getArrayOfTasks() {
		return arrayOfTasks;
	}

	public void removeSelectedFromArrayOfTasks() {
		arrayOfTasks.remove(task);
		dataChange();
	}

	public Integer getVersion() {
		return version;
	}

	public boolean isNetWorking() {
		return netWorking;
	}

	public void setNetWorking(boolean netWorking) {
		this.netWorking = netWorking;
		dataChange();
	}

	public boolean isSelfWorking() {
		return selfWorking;
	}

	public void setSelfWorking(boolean selfWorking) {
		this.selfWorking = selfWorking;
		dataChange();
	}
}
