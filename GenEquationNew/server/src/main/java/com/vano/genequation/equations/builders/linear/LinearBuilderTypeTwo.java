package com.vano.genequation.equations.builders.linear;

import com.vano.genequation.equations.Equation;
import com.vano.genequation.utils.Const;

import java.util.UUID;

import static com.vano.genequation.utils.Const.SOLVE_EQUATION_EN;
import static com.vano.genequation.utils.Random.getRandomInt;

public class LinearBuilderTypeTwo extends LinearBuilder {

    public Equation build() {
        final int reply = getRandomInt(-100, 100);
        final int a = getRandomInt(-10, -1);
        final int b = getRandomInt(1, 100);
        final int c = reply * a + b;
        return Equation.builder()
                .id(UUID.randomUUID())
                .equationType(Const.EquationType.LINEAR)
                .description(SOLVE_EQUATION_EN)
                .body(a + "x + " + b + " = " + c)
                .rightReply(reply)
                .build();
    }
}
