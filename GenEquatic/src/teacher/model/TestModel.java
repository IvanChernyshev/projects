package teacher.model;

import java.util.*;
import java.util.logging.*;

import teacher.main.*;
import common.equations.*;
import common.utils.dataObjects.*;

public class TestModel extends AbstractModel {
	private Task task;
	private List<Equation> oldEquations, newEquations;
	private int doneEquations, remainEquations, currentNumber, numberOfTasks;
	private Equation currentEquation;

	public TestModel(UserData userData) {
		super(userData);
	}

	public void doStepWithReply(int userResponseInt) {
		setResponse(userResponseInt);
		checkNoReply();
		nextStep();
		if (oldEquations.size() == 0) {
			synchronized (TestData.getInstance()) {
				TestData.getInstance().setDoneEquations(doneEquations);
				TestData.getInstance().setRemainEquations(remainEquations);
				TestData.getInstance().setNoMoreEquations(true);
			}
		}
	}

	public void nextStep() {
		synchronized (UserData.getInstance()) {
			if (!oldEquations.isEmpty()) {
				try {
					currentEquation = oldEquations.get(currentNumber);
					currentNumber++;
					sendCurrentTask();
				} catch (IndexOutOfBoundsException ex) {
					currentNumber = 0;
					currentEquation = oldEquations.get(currentNumber);
					currentNumber++;
					sendCurrentTask();
					Log.log(Level.INFO, "End of oldListOfEquations's bounds", ex);
				}
			}
		}
	}

	public void testStop() {
		formingResultData();
		TestData.getInstance().clear();
		clearModel();
	}

	private void setResponse(int response) {
		currentEquation.setUserReply(response);
		newEquations.add(currentEquation);
		oldEquations.remove(currentEquation);
		doneEquations = newEquations.size();
		remainEquations = oldEquations.size();
		currentNumber--;
	}

	private void sendCurrentTask() {
		TestData.getInstance().setCurrentEquation(currentEquation);
		TestData.getInstance().setDoneEquations(doneEquations);
		TestData.getInstance().setRemainEquations(remainEquations);
	}

	public void init() {
		synchronized (UserData.getInstance()) {
			this.task = userData.getTask();
		}
		this.oldEquations = task.getTaskEquations();
		this.newEquations = new ArrayList<Equation>();
		this.numberOfTasks = oldEquations.size();
		this.doneEquations = 0;
		this.remainEquations = oldEquations.size();
		this.currentEquation = oldEquations.get(currentNumber);
		currentNumber++;
		sendCurrentTask();
	}

	public void clearModel() {
		this.task = null;
		this.doneEquations = 0;
		this.remainEquations = 0;
		this.currentEquation = null;
		this.currentNumber = 0;
		this.numberOfTasks = 0;
		oldEquations.clear();
	}

	private void formingResultData() {
		ResultDataObject dataObject = new ResultDataObject(newEquations, numberOfTasks);
		ResultModel resultModel = new ResultModel(userData);
		resultModel.setResult(dataObject);
	}

	private void checkNoReply() {
		if (TestData.getInstance().isNewEquationsFull() == false) {
			if (newEquations.size() > 0) {
				TestData.getInstance().setNewEquationsFull(true);
			}
		}
	}
}
