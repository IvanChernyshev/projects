package teacher.view;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

import teacher.main.*;
import teacher.controller.*;
import common.utils.*;

public class TaskPanel extends AbstractPanel {

	private JPanel choosepanel;
	private JCheckBox linearBox, quadraticBox, fractionalRationalBox, combainedBox, ramdomBox;
	private JTextField linearField, quadraticField, fractionalRationalField, combainedField, ramdomField, timeField;
	private JButton startTaskButton;

	public TaskPanel(UserData userData) {
		super(userData);
	}

	@Override
	protected void setStatic() {
		mainPanel = new JPanel();
		mainPanel.setBounds(0, 0, 384, 461);
		mainPanel.setLayout(null);
		mainPanel.setBackground(Color.lightGray);
		mainPanel.setBorder(TextHandler.getBorder2());

		JLabel taskLabel = new JLabel("�������� ������� � �� ����������");
		taskLabel.setBounds(10, 11, 364, 39);
		mainPanel.add(taskLabel);
		taskLabel.setFont(TextHandler.getBigFont());

		choosepanel = new JPanel();
		choosepanel.setBounds(10, 68, 364, 226);
		mainPanel.add(choosepanel);
		choosepanel.setLayout(null);
		choosepanel.setBackground(Color.lightGray);
		choosepanel.setBorder(TextHandler.getBorder1());

		linearField = new JTextField("");
		linearField.setBounds(324, 7, 30, 31);
		choosepanel.add(linearField);
		linearField.setColumns(10);
		linearField.setHorizontalAlignment(JTextField.CENTER);
		linearField.setVisible(false);
		linearField.setEnabled(false);

		quadraticField = new JTextField("");
		quadraticField.setBounds(324, 51, 30, 31);
		choosepanel.add(quadraticField);
		quadraticField.setColumns(10);
		quadraticField.setHorizontalAlignment(JTextField.CENTER);
		quadraticField.setVisible(false);
		quadraticField.setEnabled(false);

		fractionalRationalField = new JTextField("");
		fractionalRationalField.setBounds(324, 96, 30, 31);
		choosepanel.add(fractionalRationalField);
		fractionalRationalField.setColumns(10);
		fractionalRationalField.setHorizontalAlignment(JTextField.CENTER);
		fractionalRationalField.setVisible(false);
		fractionalRationalField.setEnabled(false);

		combainedField = new JTextField("");
		combainedField.setBounds(324, 142, 30, 31);
		choosepanel.add(combainedField);
		combainedField.setColumns(10);
		combainedField.setHorizontalAlignment(JTextField.CENTER);
		combainedField.setVisible(false);
		combainedField.setEnabled(false);

		ramdomField = new JTextField("");
		ramdomField.setBounds(324, 188, 30, 31);
		choosepanel.add(ramdomField);
		ramdomField.setColumns(10);
		ramdomField.setHorizontalAlignment(JTextField.CENTER);
		ramdomField.setVisible(false);
		ramdomField.setEnabled(false);

		JLabel timeLabel = new JLabel("����� �� ���������� �������, �����");
		timeLabel.setBounds(10, 341, 313, 23);
		timeLabel.setFont(TextHandler.getMediumFont());
		mainPanel.add(timeLabel);

		timeField = new JTextField("");
		timeField.setBounds(333, 341, 30, 23);
		mainPanel.add(timeField);
		timeField.setColumns(10);
		timeField.setHorizontalAlignment(JTextField.CENTER);
	}

	@Override
	protected void setDynamic() {
		linearBox = new JCheckBox("��������");
		linearBox.setBounds(6, 7, 252, 31);
		linearBox.setFont(TextHandler.getSpetialFont());
		choosepanel.add(linearBox);
		linearBox.addItemListener(new LinearItemListener());

		quadraticBox = new JCheckBox("����������");
		quadraticBox.setBounds(6, 51, 252, 31);
		quadraticBox.setFont(TextHandler.getSpetialFont());
		choosepanel.add(quadraticBox);
		quadraticBox.addItemListener(new QuadraticItemListener());

		fractionalRationalBox = new JCheckBox("������-������������");
		fractionalRationalBox.setBounds(6, 96, 252, 31);
		fractionalRationalBox.setFont(TextHandler.getSpetialFont());
		choosepanel.add(fractionalRationalBox);
		fractionalRationalBox.addItemListener(new FractionalRationalItemListener());

		combainedBox = new JCheckBox("���������������");
		combainedBox.setBounds(6, 142, 252, 31);
		combainedBox.setFont(TextHandler.getSpetialFont());
		choosepanel.add(combainedBox);
		combainedBox.addItemListener(new CombainedItemListener());

		ramdomBox = new JCheckBox("���������");
		ramdomBox.setBounds(6, 188, 252, 31);
		ramdomBox.setFont(TextHandler.getSpetialFont());
		choosepanel.add(ramdomBox);
		ramdomBox.addItemListener(new RamdomBoxItemListener());

		JButton saveTaskButton = new JButton("���������");
		saveTaskButton.setBounds(39, 389, 121, 49);
		saveTaskButton.setFont(TextHandler.getLittleFont());
		mainPanel.add(saveTaskButton);
		saveTaskButton.addActionListener(new SaveTaskButtonActionListener());

		startTaskButton = new JButton();
		startTaskButton.setBounds(216, 389, 121, 49);
		startTaskButton.setFont(TextHandler.getLittleFont());
		mainPanel.add(startTaskButton);
		startTaskButton.addActionListener(new StartTaskButtonActionListener());
	}

	public class LinearItemListener implements ItemListener {
		@Override
		public void itemStateChanged(ItemEvent arg0) {
			boolean selected = linearBox.isSelected();
			linearField.setVisible(selected);
			linearField.setEnabled(selected);
			linearField.requestFocus();
			if (selected == false) {
				linearField.setText("");
			}
		}
	}

	public class QuadraticItemListener implements ItemListener {
		@Override
		public void itemStateChanged(ItemEvent arg0) {
			boolean selected = quadraticBox.isSelected();
			quadraticField.setVisible(selected);
			quadraticField.setEnabled(selected);
			quadraticField.requestFocus();
			if (selected == false) {
				quadraticField.setText("");
			}
		}
	}

	public class FractionalRationalItemListener implements ItemListener {
		@Override
		public void itemStateChanged(ItemEvent arg0) {
			boolean selected = fractionalRationalBox.isSelected();
			fractionalRationalField.setVisible(selected);
			fractionalRationalField.setEnabled(selected);
			fractionalRationalField.requestFocus();
			if (selected == false) {
				fractionalRationalField.setText("");
			}
		}
	}

	public class CombainedItemListener implements ItemListener {
		@Override
		public void itemStateChanged(ItemEvent arg0) {
			boolean selected = combainedBox.isSelected();
			combainedField.setVisible(selected);
			combainedField.setEnabled(selected);
			combainedField.requestFocus();
			if (selected == false) {
				combainedField.setText("");
			}
		}
	}

	public class RamdomBoxItemListener implements ItemListener {
		@Override
		public void itemStateChanged(ItemEvent arg0) {
			boolean selected = ramdomBox.isSelected();
			ramdomField.setVisible(selected);
			ramdomField.setEnabled(selected);
			ramdomField.requestFocus();
			if (selected == false) {
				ramdomField.setText("");
			}
		}
	}

	public class SaveTaskButtonActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			TaskController.getInstance().saveTask(getUserInput());
		}
	}

	public class StartTaskButtonActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			TaskController.getInstance().startTask(getUserInput());
		}
	}

	private ArrayList<String> getUserInput() {
		ArrayList<String> userInput = new ArrayList<String>();
		userInput.add(linearField.getText());
		userInput.add(quadraticField.getText());
		userInput.add(fractionalRationalField.getText());
		userInput.add(combainedField.getText());
		userInput.add(ramdomField.getText());
		userInput.add(timeField.getText());
		return userInput;
	}

	@Override
	public void clearAll() {
		linearField.setText("");
		quadraticField.setText("");
		fractionalRationalField.setText("");
		combainedField.setText("");
		ramdomField.setText("");
		timeField.setText("");
		linearBox.setSelected(false);
		quadraticBox.setSelected(false);
		fractionalRationalBox.setSelected(false);
		combainedBox.setSelected(false);
		ramdomBox.setSelected(false);
	}

	@Override
	public void updateView() {
		if (UserData.getInstance().isSelfWorking()) {
			startTaskButton.setText("������");
		} else {
			startTaskButton.setText("���������");
		}
	}
}
