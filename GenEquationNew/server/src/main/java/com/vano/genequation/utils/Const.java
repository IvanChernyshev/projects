package com.vano.genequation.utils;

public class Const {
    public final static String SOLVE_EQUATION_EN = "Solve the equation";
    public final static String SOLVE_EQUATION_WITH_MAJOR_ROOT_EN = "Solve the equation. Write major root in response";
    public final static String SOLVE_EQUATION_WITH_MINOR_ROOT_EN = "Solve the equation. Write minor root in response";
    public final static String SOLVED_CORRECTLY_EN = "Correct: ";
    public final static String RIGHT_REPLY_EN = "True reply: ";
    public final static String USER_REPLY_EN = "User's reply: ";

    public enum EquationType {
        LINEAR,
        COMBINED,
        QUADRATIC,
        FRACTIONAL_RATIONAL
    }
}
