package common.equations;

import java.util.*;

import common.equations.fa�tory.*;
import common.utils.*;

public class SetUpEquations {
	private List<EquationCreator> arrayOfCreators;
	private List<Equation> arrayOfEquations;
	private final static int LINEAR_SCOPE_BEGINNING = -1;
	private final static int LINEAR_SCOPE_END = 1;
	private final static int QUADRATIC_SCOPE_BEGINNING = 1;
	private final static int QUADRATIC_SCOPE_END = 8;
	private final static int FRACTIONAL_SCOPE_BEGINNING = 8;
	private final static int FRACTIONAL_SCOPE_END = 11;
	private final static int COMBINED_SCOPE_BEGINNING = 11;
	private final static int COMBINED_SCOPE_END = 13;
	private final static int RANDOM_SCOPE_BEGINNING = -1;
	private final static int RANDOM_SCOPE_END = 13;

	public SetUpEquations() {
		this.arrayOfCreators = new ArrayList<EquationCreator>();
		this.arrayOfEquations = new ArrayList<Equation>();
		this.arrayOfCreators.add(new LinearType1Creator());
		this.arrayOfCreators.add(new LinearType2Creator());
		this.arrayOfCreators.add(new QuadraticType1Creator());
		this.arrayOfCreators.add(new QuadraticType2Creator());
		this.arrayOfCreators.add(new QuadraticType3Creator());
		this.arrayOfCreators.add(new QuadraticType4Creator());
		this.arrayOfCreators.add(new QuadraticType5Creator());
		this.arrayOfCreators.add(new QuadraticType6Creator());
		this.arrayOfCreators.add(new QuadraticType7Creator());
		this.arrayOfCreators.add(new FractionalRationalType1Creator());
		this.arrayOfCreators.add(new FractionalRationalType2Creator());
		this.arrayOfCreators.add(new FractionalRationalType3Creator());
		this.arrayOfCreators.add(new CombinedType1Creator());
		this.arrayOfCreators.add(new CombinedType2Creator());
	}

	public void setUp(int linear, int quadratic, int fractional, int combined, int random) {
		setUpEquation(linear, LINEAR_SCOPE_BEGINNING, LINEAR_SCOPE_END);
		setUpEquation(quadratic, QUADRATIC_SCOPE_BEGINNING, QUADRATIC_SCOPE_END);
		setUpEquation(fractional, FRACTIONAL_SCOPE_BEGINNING, FRACTIONAL_SCOPE_END);
		setUpEquation(combined, COMBINED_SCOPE_BEGINNING, COMBINED_SCOPE_END);
		setUpEquation(random, RANDOM_SCOPE_BEGINNING, RANDOM_SCOPE_END);
	}

	private void setUpEquation(int count, int beginning, int end) {
		for (int i = 0; i < count; i++) {
			int num = RandomGenerator.getRandom(beginning, end);
			Equation equation = arrayOfCreators.get(num).create();
			arrayOfEquations.add(equation);
		}
	}

	public List<Equation> getList() {
		return arrayOfEquations;
	}
}
