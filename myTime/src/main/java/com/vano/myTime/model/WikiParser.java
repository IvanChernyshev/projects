package com.vano.myTime.model;

import java.io.*;
import java.net.*;
import java.util.*;

import javax.swing.*;
import javax.xml.parsers.*;

import org.w3c.dom.*;
import org.xml.sax.*;

import com.vano.myTime.view.*;

import info.bliki.wiki.model.*;

class WikiParser {

    void parseResponse(String age) throws UnsupportedOperationException {
        String endPoint = "https://en.wikipedia.org/w/api.php?";
        Scanner scanner = null;
        BufferedWriter writer = null;
        try {
            URL url = new URL(endPoint + "action=parse&page=" + age + "&prop=wikitext&redirects=true&format=xml");
            scanner = new Scanner(url.openStream());
            writer = new BufferedWriter(new FileWriter(new File("C:\\Users\\HOME\\Desktop\\wiki.xml")));
            while (scanner.hasNextLine()) {
                String string = scanner.nextLine();
                writer.write(string);
            }
            writer.flush();
            parseXML();
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, "Ошибка в приеме ответа с удаленного ресурса");
            e.printStackTrace();
            throw new UnsupportedOperationException();
        } finally {
            if (scanner != null) {
                scanner.close();
            }
            try {
                if (writer != null) {
                    writer.close();
                }
            } catch (IOException e) {
                JOptionPane.showMessageDialog(null, "Не удалось закрыть объект BufferedWriter ");
                e.printStackTrace();
            }
        }
    }

    private void parseXML() throws UnsupportedOperationException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;
        try {
            builder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException ex) {
            JOptionPane.showMessageDialog(null, "Ошибка в создании объекта DocumentBuilder");
            ex.printStackTrace();
        }
        try {
            if (builder != null) {
                Document document = builder.parse(new File("C:\\Users\\HOME\\Desktop\\wiki.xml"));
                findData(document);
            }
        } catch (SAXException e) {
            JOptionPane.showMessageDialog(null, "Ошибка в распозновании файла xml");
            e.printStackTrace();
            throw new UnsupportedOperationException();
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, "Ошибка в создании объекта Document ");
            e.printStackTrace();
        }
    }

    private void findData(Document document) {
        StringBuilder builder = new StringBuilder();
        NodeList nList = document.getElementsByTagName("api");
        for (int i = 0; i < nList.getLength(); i++) {
            Node node = nList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;
                String wikiText = element.getTextContent();
                builder.append(wikiText);
            }
        }
        bliki(builder.toString());
    }

    private void bliki(String text) {
        String htmlText = WikiModel.toHtml(text);
        String result = HtmlParser.parseHTML(htmlText);
        new SecondaryFrame(result).start();
    }
}
