package com.vano.RevelationServer;

import java.io.*;
import java.util.*;
import javax.swing.*;

public class MyText {
	private ArrayList<ArrayList<String>> books;
	private static volatile MyText instance;

	private MyText() {
		books = new ArrayList<ArrayList<String>>();
		parseText();
	}

	public final static MyText getInstance() {
		if (instance == null) {
			synchronized (MyText.class) {
				if (instance == null) {
					instance = new MyText();
				}
			}
		}
		return instance;
	}

	private void parseText() {
		ArrayList<String> chapters = null;
		String stringText = getText();
		String[] splitText = stringText.split("Глава" + "\n");
		for (String s : splitText) {
			String[] mass = s.split("\n");
			chapters = new ArrayList<String>();
			for (int i = 0; i < mass.length; i++) {
				chapters.add(mass[i]);
			}
			books.add(chapters);
		}
	}

	private String getText() {
		StringBuilder stringsFromFile = new StringBuilder();
		BufferedReader reader = new BufferedReader(
				new InputStreamReader(MyText.class.getResourceAsStream("/text/apoc.txt")));
		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				stringsFromFile = stringsFromFile.append(line + "\n");
			}
		} catch (IOException ex) {
			ex.printStackTrace();
			JOptionPane.showMessageDialog(null, "Не удалось прочитать данные из файла");
		} finally {
			try {
				if (reader != null) {
					reader.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
				JOptionPane.showMessageDialog(null, "Не удалось закрыть BufferedReader");
			}
		}
		return stringsFromFile.toString();
	}

	public ArrayList<ArrayList<String>> getBooks() {
		return books;
	}
}
