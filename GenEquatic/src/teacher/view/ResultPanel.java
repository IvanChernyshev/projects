package teacher.view;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

import teacher.main.*;
import common.constants.*;
import common.utils.*;
import common.utils.dataObjects.Result;
import teacher.controller.*;

public class ResultPanel extends AbstractPanel {
	private JLabel nameLabel, groupLabel, timeLabel, allTasksLabel, doneLabel, rightLabel, ratingLabel;
	private Result result;

	public ResultPanel(UserData userData) {
		super(userData);
	}

	@Override
	protected void setStatic() {
		mainPanel = new JPanel();
		mainPanel.setBounds(0, 0, 384, 471);
		mainPanel.setBackground(Color.lightGray);
		mainPanel.setBorder(TextHandler.getBorder2());
		mainPanel.setLayout(null);

		JPanel topPanel = new JPanel();
		topPanel.setBounds(10, 11, 364, 50);
		mainPanel.add(topPanel);
		topPanel.setLayout(null);
		topPanel.setBackground(Color.lightGray);
		topPanel.setBorder(TextHandler.getBorder1());

		JLabel topLabel = new JLabel("���������� ���������� �����");
		topLabel.setBounds(28, 11, 315, 28);
		topLabel.setFont(TextHandler.getBigFont());
		topPanel.add(topLabel);

	}

	@Override
	protected void setDynamic() {
		JPanel lowPanel = new JPanel();
		lowPanel.setBounds(10, 72, 364, 285);
		mainPanel.add(lowPanel);
		lowPanel.setLayout(null);
		lowPanel.setBackground(Color.lightGray);
		lowPanel.setBorder(TextHandler.getBorder1());

		nameLabel = new JLabel();
		nameLabel.setBounds(10, 11, 344, 28);
		lowPanel.add(nameLabel);
		nameLabel.setFont(TextHandler.getMediumFont());
		groupLabel = new JLabel();
		groupLabel.setBounds(10, 50, 344, 28);
		lowPanel.add(groupLabel);
		groupLabel.setFont(TextHandler.getMediumFont());

		timeLabel = new JLabel();
		timeLabel.setBounds(10, 89, 344, 28);
		lowPanel.add(timeLabel);
		timeLabel.setFont(TextHandler.getMediumFont());

		allTasksLabel = new JLabel();
		allTasksLabel.setBounds(10, 128, 344, 28);
		lowPanel.add(allTasksLabel);
		allTasksLabel.setFont(TextHandler.getMediumFont());

		doneLabel = new JLabel();
		doneLabel.setBounds(10, 167, 344, 28);
		lowPanel.add(doneLabel);
		doneLabel.setFont(TextHandler.getMediumFont());

		rightLabel = new JLabel();
		rightLabel.setBounds(10, 206, 344, 28);
		lowPanel.add(rightLabel);
		rightLabel.setFont(TextHandler.getMediumFont());

		ratingLabel = new JLabel();
		ratingLabel.setBounds(10, 245, 344, 28);
		lowPanel.add(ratingLabel);
		ratingLabel.setFont(TextHandler.getMediumFont());

		JButton endButton = new JButton("�� �������");
		endButton.setBounds(10, 386, 135, 50);
		mainPanel.add(endButton);
		endButton.setFont(TextHandler.getLittleFont());
		endButton.addActionListener(new EndActionListener());

		JButton detailButton = new JButton("�����������");
		detailButton.setBounds(239, 386, 135, 50);
		mainPanel.add(detailButton);
		detailButton.setFont(TextHandler.getLittleFont());
		detailButton.addActionListener(new DetailActionListener());

	}

	public class EndActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (UserData.getInstance().isSelfWorking()) {
				CommonController.getInstance().clearPanel(PanelsConstants.RESULT_PANEL);
				CommonController.getInstance().selectPanel(PanelsConstants.TYPE_OF_WORK_PANEL);
			} else {
				CommonController.getInstance().clearPanel(PanelsConstants.RESULT_PANEL);
				CommonController.getInstance().selectPanel(PanelsConstants.ANSWER_PANEL);
			}
		}
	}

	public class DetailActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			CommonController.getInstance().selectPanel(PanelsConstants.DETAIL_PANEL);
		}
	}

	@Override
	public void update(Observable o, Object arg) {
		if (o instanceof UserData) {
			UserData userData = (UserData) o;
			this.result = userData.getResult();
		}
	}

	@Override
	public void clearAll() {
		this.nameLabel.setText("");
		this.groupLabel.setText("");
		this.timeLabel.setText("");
		this.allTasksLabel.setText("");
		this.doneLabel.setText("");
		this.rightLabel.setText("");
		this.ratingLabel.setText("");
	}

	@Override
	public void updateView() {
		nameLabel.setText("���: " + result.getPerson().getFirstName() + " " + result.getPerson().getSecondName());
		groupLabel.setText("������: " + result.getPerson().getGroup());
		timeLabel.setText("����� ����������, �����: " + result.getMin() + ":" + result.getSec());
		allTasksLabel.setText("�������� �������: " + result.getAllTask());
		doneLabel.setText("�� ��� ���������: " + result.getDone().size());
		rightLabel.setText("���������� �������: " + result.getRightTask() + " (" + result.getRating() + "%)");
		ratingLabel.setText("����: " + result.getResume());
	}
}
