package student.controller;

import student.main.*;
import common.constants.*;
import common.utils.dataObjects.*;
import student.model.*;

public class AnswerController extends AbstractController {

	private static volatile AnswerController instance = null;

	private AnswerController() {
	}

	public final static AnswerController getInstance() {
		if (instance == null) {
			synchronized (AnswerController.class) {
				if (instance == null) {
					instance = new AnswerController();
				}
			}
		}
		return instance;
	}

	public void setTask(Task task) {
		AnswerModel model = new AnswerModel(UserData.getInstance());
		model.setTask(task);
		selectPanel(PanelsConstants.START_TEST_PANEL);
	}

	public void returnToMainPanel() {
		selectPanel(PanelsConstants.TYPE_OF_WORK_PANEL);
	}

	public void deleteTask() {
		panels.get(PanelsConstants.ANSWER_PANEL).clearAll();
	}
}
