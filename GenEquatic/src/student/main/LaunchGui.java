package student.main;

import java.util.*;
import java.util.logging.*;

import common.constants.*;
import student.view.*;

public class LaunchGui {

	public static void main(String[] args) {
		initLogger();
		MainFrame frame = new MainFrame("GenEquatic 'S' v1.02");
		List<AbstractPanel> views = new SetUpView(UserData.getInstance()).getListOfPanels();
		SetUpController setUpController = new SetUpController();
		setUpController.initControllers(frame, views);
		frame.setPanel(views.get(PanelsConstants.JOIN_PANEL));
		frame.repaint();
	}

	private static void initLogger() {
		try {
			LogManager.getLogManager()
					.readConfiguration(LaunchGui.class.getResourceAsStream("/student/logging.properties.txt"));
		} catch (Exception ex) {
			System.err.println("Couldn't set up logging configurations");
			ex.printStackTrace();
		}
	}
}
