package student.controller;

import student.main.*;
import common.constants.*;
import student.model.*;

public class TypeOfWorkController extends AbstractController {

	private static volatile TypeOfWorkController instance = null;

	private TypeOfWorkController() {
	}

	public final static TypeOfWorkController getInstance() {
		if (instance == null) {
			synchronized (TypeOfWorkController.class) {
				if (instance == null) {
					instance = new TypeOfWorkController();
				}
			}
		}
		return instance;
	}

	public void selfWorking() {
		TypeOfWorkModel model = new TypeOfWorkModel(UserData.getInstance());
		model.setTypeOfWork(true);
		selectPanel(PanelsConstants.TASK_PANEL);
	}

	public void workingWithTeacher() {
		TypeOfWorkModel model = new TypeOfWorkModel(UserData.getInstance());
		model.setTypeOfWork(false);
		selectPanel(PanelsConstants.ANSWER_PANEL);
	}

	public void setSelfWorking(boolean selfWorking) {
		TypeOfWorkModel model = new TypeOfWorkModel(UserData.getInstance());
		model.setTypeOfWork(selfWorking);
	}
}
