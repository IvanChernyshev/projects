package common.utils.dataObjects;

public class LoginDataObject {
	private final String firstName, secondName, group, ipAddress;

	public LoginDataObject(String firstName, String secondName, String group, String ipAddress) {
		this.firstName = firstName;
		this.secondName = secondName;
		this.group = group;
		this.ipAddress = ipAddress;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getSecondName() {
		return secondName;
	}

	public String getGroup() {
		return group;
	}

	public String getIpAddress() {
		return ipAddress;
	}
}
