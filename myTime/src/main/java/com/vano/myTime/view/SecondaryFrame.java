package com.vano.myTime.view;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import java.awt.*;

public class SecondaryFrame {
    private String description;

    public SecondaryFrame(String description) {
        this.description = description;
    }

    public void start() {
        JFrame frame = new JFrame("AgeEvents");
        frame.setVisible(true);
        frame.setBounds(200, 200, 400, 360);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.getContentPane().setLayout(new BorderLayout(0, 0));

        Panel panel = new Panel();
        frame.getContentPane().add(panel, BorderLayout.CENTER);
        panel.setLayout(null);

        JLabel topLabel = new JLabel("Main events in this year :");
        topLabel.setBounds(10, 11, 374, 39);
        topLabel.setForeground(Color.green);
        topLabel.setFont(new Font("Verdana", Font.ITALIC, 25));
        topLabel.setHorizontalAlignment(JLabel.CENTER);
        topLabel.setBackground(Color.gray);
        panel.add(topLabel);

        JTextArea textArea = new JTextArea(description);
        textArea.setBounds(10, 61, 364, 250);
        textArea.setForeground(Color.green);
        textArea.setFont(new Font("Verdana", Font.ITALIC, 15));
        textArea.setBackground(Color.gray);
        textArea.setEditable(false);
        textArea.setWrapStyleWord(true);
        textArea.setLineWrap(true);

        JScrollPane scroller = new JScrollPane(textArea);
        scroller.setBounds(10, 61, 374, 260);
        scroller.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        scroller.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        panel.add(scroller);

        frame.setResizable(false);
        frame.revalidate();
        frame.repaint();
    }
}
