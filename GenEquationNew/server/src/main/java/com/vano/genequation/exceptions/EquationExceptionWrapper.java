package com.vano.genequation.exceptions;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EquationExceptionWrapper {
    private String messageError;
    private String cause;
    private long timestamp;

    public EquationExceptionWrapper(Exception cause) {
        this.messageError = cause.getMessage();
        this.cause = cause.getClass().getName();
        this.timestamp = System.currentTimeMillis();
    }
}
