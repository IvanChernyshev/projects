package common.utils.dataObjects;

import common.equations.*;

public class TestData {
	private static volatile TestData instance = null;
	private Equation currentEquation;
	private boolean noMoreEquations, isNewEquationsFull;
	private int secondsDone, doneEquations, remainEquations;

	private TestData() {
	}

	public final static TestData getInstance() {
		if (instance == null) {
			synchronized (TestData.class) {
				if (instance == null) {
					instance = new TestData();
				}
			}
		}
		return instance;
	}

	public void clear() {
		this.currentEquation = null;
		this.secondsDone = 0;
		this.doneEquations = 0;
		this.remainEquations = 0;
		this.noMoreEquations = false;
		this.isNewEquationsFull = false;
	}

	public boolean isNoMoreEquations() {
		return noMoreEquations;
	}

	public void setNoMoreEquations(boolean noMoreEquations) {
		this.noMoreEquations = noMoreEquations;
	}

	public synchronized int getSecondsDone() {
		return secondsDone;
	}

	public synchronized void setSecondsDone(int secondsDone) {
		this.secondsDone = secondsDone;
	}

	public Equation getCurrentEquation() {
		return currentEquation;
	}

	public void setCurrentEquation(Equation currentEquation) {
		this.currentEquation = currentEquation;
	}

	public int getDoneEquations() {
		return doneEquations;
	}

	public void setDoneEquations(int doneEquations) {
		this.doneEquations = doneEquations;
	}

	public int getRemainEquations() {
		return remainEquations;
	}

	public void setRemainEquations(int remainEquations) {
		this.remainEquations = remainEquations;
	}

	public boolean isNewEquationsFull() {
		return isNewEquationsFull;
	}

	public void setNewEquationsFull(boolean isNewEquationsFull) {
		this.isNewEquationsFull = isNewEquationsFull;
	}
}
