package com.vano.Currency;
import java.io.*;
import java.net.*;
import java.util.*;

import javax.swing.*;
import javax.xml.parsers.*;

import org.w3c.dom.*;
import org.xml.sax.*;

public class TextHandler {
	private Document document;
	private final String urlAddress = "http://www.cbr.ru/scripts/XML_daily_eng.asp";
	private final String address = "currency.xml";
	private Vector<String> arrayOfCurrency;
	private String value, nominal;

	public void parseResponse() {
		Scanner scanner = null;
		BufferedWriter writer = null;
		try {
			URL url = new URL(urlAddress);
			scanner = new Scanner(url.openStream());
			writer = new BufferedWriter(new FileWriter(new File(address)));
			while (scanner.hasNextLine()) {
				String string = scanner.nextLine();
				writer.write(string);
			}
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Ошибка в приеме ответа с удаленного ресурса");
			e.printStackTrace();
		} finally {
			scanner.close();
			try {
				writer.flush();
				writer.close();
			} catch (IOException e) {
				JOptionPane.showMessageDialog(null, "Не удалось закрыть объект BufferedWriter");
				e.printStackTrace();
			}
		}
	}

	public void parseXML() throws UnsupportedOperationException {
		if (document == null) {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = null;
			try {
				builder = factory.newDocumentBuilder();
			} catch (ParserConfigurationException e1) {
				JOptionPane.showMessageDialog(null, "Ошибка в создании объекта DocumentBuilder");
				e1.printStackTrace();
			}
			try {
				document = builder.parse(new File(address));
			} catch (SAXException e) {
				JOptionPane.showMessageDialog(null, "Ошибка в распозновании файла xml");
				e.printStackTrace();
				throw new UnsupportedOperationException();
			} catch (IOException e) {
				JOptionPane.showMessageDialog(null, "Ошибка в создании объекта Document");
				e.printStackTrace();
			}
		}
	}

	public Vector<String> createCurrencyTitles() {
		arrayOfCurrency = new Vector<String>();
		NodeList nList = document.getElementsByTagName("Valute");
		for (int i = 0; i < nList.getLength(); i++) {
			Node node = nList.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				Element element = (Element) node;
				String title = element.getElementsByTagName("Name").item(0).getTextContent();
				arrayOfCurrency.add(title);
			}
		}
		return arrayOfCurrency;
	}

	public void findData(String currency) {
		NodeList nList = document.getElementsByTagName("Valute");
		for (int i = 0; i < nList.getLength(); i++) {
			Node node = nList.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				Element element = (Element) node;
				if (element.getElementsByTagName("Name").item(0).getTextContent().equals(currency)) {
					value = element.getElementsByTagName("Value").item(0).getTextContent();
					nominal = element.getElementsByTagName("Nominal").item(0).getTextContent();
					break;
				}
			}
		}
	}

	public String getValue() {
		return value;
	}

	public String getNominal() {
		return nominal;
	}
}
