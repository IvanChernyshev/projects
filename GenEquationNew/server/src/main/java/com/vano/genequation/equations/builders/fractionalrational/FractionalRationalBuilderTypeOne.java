package com.vano.genequation.equations.builders.fractionalrational;

import com.vano.genequation.equations.Equation;
import com.vano.genequation.utils.Const;

import java.util.UUID;

import static com.vano.genequation.utils.Const.SOLVE_EQUATION_EN;
import static com.vano.genequation.utils.Random.getRandomInt;

public class FractionalRationalBuilderTypeOne extends FractionalRationalBuilder {

    @Override
    public Equation build() {
        final int reply = getRandomInt(1, 11);
        final int a = getRandomInt(2, 7);
        final int c = getRandomInt(2, 9);
        final int d = getRandomInt(4, 10);
        final int b = reply * (d * c - a);
        return Equation.builder()
                .id(UUID.randomUUID())
                .equationType(Const.EquationType.FRACTIONAL_RATIONAL)
                .description(SOLVE_EQUATION_EN)
                .body("(" + a + "x + " + b + ")/" + c + "x = " + d)
                .rightReply(reply)
                .build();
    }
}
