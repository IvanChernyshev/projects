package common.utils;

public class ResumeHandler {
	public final static String EXCELLENT_MARK = "���� ������ ������!";
	public final static String EXCELENT_WITH_MINUS_MARK = "�������, ����!";
	public final static String GOOD_MARK = "�����";
	public final static String GOOD_WITH_MINUS_MARK = "����� ����������!";
	public final static String BAD_MARK = "�����������!";

	public static String getResume(int rating) {
		String resume = null;
		if (rating >= 95) {
			resume = EXCELLENT_MARK;
		}
		if (rating >= 80 && rating < 95) {
			resume = EXCELENT_WITH_MINUS_MARK;
		}
		if (rating >= 60 && rating < 80) {
			resume = GOOD_MARK;
		}
		if (rating >= 40 && rating < 60) {
			resume = GOOD_WITH_MINUS_MARK;
		}
		if (rating < 40) {
			resume = BAD_MARK;
		}
		return resume;
	}
}
