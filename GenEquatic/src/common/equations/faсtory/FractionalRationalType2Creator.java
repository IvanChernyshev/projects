package common.equations.fa�tory;

import common.equations.*;
import common.utils.*;

public class FractionalRationalType2Creator extends EquationCreator {

	public Equation create() {
		int x1 = RandomGenerator.getRandom(1, 11);
		int a = RandomGenerator.getRandom(2, 7);
		int c = RandomGenerator.getRandom(2, 9);
		int d = RandomGenerator.getRandom(-10, -1);
		int b = x1 * (d * c - a);
		String task = " ������ ���������: \n" + " " + "(" + a + "x " + b + ")/" + c + "x = " + d;
		return new FractionalRational(a, b, c, d, 0, 0, x1, 0, task);
	}
}
