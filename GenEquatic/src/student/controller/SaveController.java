package student.controller;

import student.main.*;
import common.constants.*;
import common.utils.dataObjects.*;
import student.model.*;

public class SaveController extends AbstractController {

	private static volatile SaveController instance = null;

	private SaveController() {
	}

	public final static SaveController getInstance() {
		if (instance == null) {
			synchronized (SaveController.class) {
				if (instance == null) {
					instance = new SaveController();
				}
			}
		}
		return instance;
	}

	public void saveData(SaveDataObject dataObject) {
		SaveModel saveModel = new SaveModel(UserData.getInstance());
		saveModel.init(dataObject);
	}

	public void selfWorking() {
		selectPanel(PanelsConstants.START_TEST_PANEL);
		clearPanel(PanelsConstants.SAVE_TASK_PANEL);
	}

	public void returnToMainPanel() {
		selectPanel(PanelsConstants.TYPE_OF_WORK_PANEL);
		clearPanel(PanelsConstants.SAVE_TASK_PANEL);
	}
}
