package student.view;

import java.util.*;
import javax.swing.*;

import student.main.*;

public abstract class AbstractPanel implements Observer {

	protected JPanel mainPanel;

	public AbstractPanel(UserData userData) {
		userData.addObserver(this);
		init();
	}

	private void init() {
		setStatic();
		setDynamic();
	}

	public JPanel getPanel() {
		updateView();
		return mainPanel;
	}

	public void clearAll() {
	}

	public void clear() {
	}

	public void updateView() {
	}

	public void end() {
	}

	@Override
	public void update(Observable o, Object arg) {
	}

	protected abstract void setStatic();

	protected abstract void setDynamic();
}
