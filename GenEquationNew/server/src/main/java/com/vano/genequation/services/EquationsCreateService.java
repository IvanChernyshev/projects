package com.vano.genequation.services;

import com.vano.genequation.equations.Equation;
import com.vano.genequation.equations.builders.EquationBuilder;
import com.vano.genequation.equations.builders.combained.CombinedBuilderTypeOne;
import com.vano.genequation.equations.builders.combained.CombinedBuilderTypeTwo;
import com.vano.genequation.equations.builders.fractionalrational.FractionalRationalBuilderTypeOne;
import com.vano.genequation.equations.builders.fractionalrational.FractionalRationalBuilderTypeThree;
import com.vano.genequation.equations.builders.fractionalrational.FractionalRationalBuilderTypeTwo;
import com.vano.genequation.equations.builders.linear.LinearBuilderTypeOne;
import com.vano.genequation.equations.builders.linear.LinearBuilderTypeTwo;
import com.vano.genequation.equations.builders.quadratic.QuadraticBuilderTypeFive;
import com.vano.genequation.equations.builders.quadratic.QuadraticBuilderTypeFour;
import com.vano.genequation.equations.builders.quadratic.QuadraticBuilderTypeOne;
import com.vano.genequation.equations.builders.quadratic.QuadraticBuilderTypeSeven;
import com.vano.genequation.equations.builders.quadratic.QuadraticBuilderTypeSix;
import com.vano.genequation.equations.builders.quadratic.QuadraticBuilderTypeThree;
import com.vano.genequation.equations.builders.quadratic.QuadraticBuilderTypeTwo;
import com.vano.genequation.models.CreationOrder;
import com.vano.genequation.repositories.EquationRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.vano.genequation.utils.Random.getRandomInt;

@Service
@Slf4j
public class EquationsCreateService {

    private final List<EquationBuilder> linearBuilders = new ArrayList<>();
    private final List<EquationBuilder> combinedBuilders = new ArrayList<>();
    private final List<EquationBuilder> fractionalRationalBuilders = new ArrayList<>();
    private final List<EquationBuilder> quadraticBuilders = new ArrayList<>();
    private final EquationRepository equationRepository;


    public EquationsCreateService(EquationRepository equationRepository) {
        initLinearBuilders(linearBuilders);
        initCombinedBuilders(combinedBuilders);
        initFractionalRationalBuilders(fractionalRationalBuilders);
        initQuadraticBuilders(quadraticBuilders);
        this.equationRepository = equationRepository;
    }

    public List<Equation> createEquations(CreationOrder order) {
        final List<Equation> equations = Stream.of(createLinear(order.getLinearCount()),
                createCombined(order.getCombinedCount()),
                createQuadratic(order.getQuadraticCount()),
                createFractionalRational(order.getFractionalCount()),
                createRandom(order.getRandomCount()))
                .flatMap(Collection::stream)
                .sorted(Comparator.comparing(Equation::getEquationType))
                .collect(Collectors.toList());
        setSequenceNumber(equations);
        log.debug("Equations created successfully");
        equationRepository.saveAll(equations);
        return equations;
    }

    private List<Equation> createLinear(int count) {
        return createEquations(count, linearBuilders);
    }

    private List<Equation> createQuadratic(int count) {
        return createEquations(count, quadraticBuilders);
    }

    private List<Equation> createFractionalRational(int count) {
        return createEquations(count, fractionalRationalBuilders);
    }

    private List<Equation> createCombined(int count) {
        return createEquations(count, combinedBuilders);
    }

    private List<Equation> createRandom(int count) {
        final List<EquationBuilder> collectedBuilders = Stream.of(linearBuilders,
                quadraticBuilders,
                fractionalRationalBuilders,
                combinedBuilders)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
        return createEquations(count, collectedBuilders);
    }

    private List<Equation> createEquations(int count, List<EquationBuilder> builders) {
        final List<Equation> equations = new ArrayList<>();
        if (count > 0) {
            for (int i = 0; i < count; i++) {
                final int num = getRandomInt(-1, (builders.size() - 1));
                final Equation equation = builders.get(num).build();
                equations.add(equation);
            }
        }
        return equations;
    }

    private void setSequenceNumber(List<Equation> equations) {
        Optional.of(equations)
                .ifPresent(eq -> eq.forEach(equation -> {
                    equation.setSequenceNumber(equations.indexOf(equation) + 1);
                }));
    }

    private void initLinearBuilders(List<EquationBuilder> linearBuilders) {
        linearBuilders.add(new LinearBuilderTypeOne());
        linearBuilders.add(new LinearBuilderTypeTwo());
    }

    private void initCombinedBuilders(List<EquationBuilder> combinedBuilders) {
        combinedBuilders.add(new CombinedBuilderTypeOne());
        combinedBuilders.add(new CombinedBuilderTypeTwo());
    }

    private void initQuadraticBuilders(List<EquationBuilder> quadraticBuilders) {
        quadraticBuilders.add(new QuadraticBuilderTypeOne());
        quadraticBuilders.add(new QuadraticBuilderTypeTwo());
        quadraticBuilders.add(new QuadraticBuilderTypeThree());
        quadraticBuilders.add(new QuadraticBuilderTypeFour());
        quadraticBuilders.add(new QuadraticBuilderTypeFive());
        quadraticBuilders.add(new QuadraticBuilderTypeSix());
        quadraticBuilders.add(new QuadraticBuilderTypeSeven());
    }

    private void initFractionalRationalBuilders(List<EquationBuilder> fractionalRationalBuilders) {
        fractionalRationalBuilders.add(new FractionalRationalBuilderTypeOne());
        fractionalRationalBuilders.add(new FractionalRationalBuilderTypeTwo());
        fractionalRationalBuilders.add(new FractionalRationalBuilderTypeThree());
    }
}
