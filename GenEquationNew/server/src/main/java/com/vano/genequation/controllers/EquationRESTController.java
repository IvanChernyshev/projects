package com.vano.genequation.controllers;

import com.vano.genequation.equations.Equation;
import com.vano.genequation.exceptions.EquationExceptionWrapper;
import com.vano.genequation.models.CreationOrder;
import com.vano.genequation.repositories.EquationRepository;
import com.vano.genequation.services.EquationsCreateService;
import com.vano.genequation.utils.Const;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.ConstraintViolationException;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Positive;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("api/v1/equations")
@Validated
@Slf4j
public class EquationRESTController {

    private final EquationsCreateService equationsCreateService;
    private final EquationRepository equationRepository;

    public EquationRESTController(EquationsCreateService equationsCreateService,
                                  EquationRepository equationRepository) {
        this.equationsCreateService = equationsCreateService;
        this.equationRepository = equationRepository;
    }

    @GetMapping("/linear/{count}")
    public List<Equation> createLinear(@PathVariable @Positive int count) {
        log.debug("New incoming GET request to create {} linear equations", count);
        return equationsCreateService.createEquations(CreationOrder.builder()
                .linearCount(count)
                .build());
    }

    @GetMapping("/quadratic/{count}")
    public List<Equation> createQuadratic(@PathVariable @Positive int count) {
        log.debug("New incoming GET request to create {} quadratic equations", count);
        return equationsCreateService.createEquations(CreationOrder.builder()
                .quadraticCount(count)
                .build());
    }

    @GetMapping("/combined/{count}")
    public List<Equation> createCombined(@PathVariable @Positive int count) {
        log.debug("New incoming GET request to create {} combined equations", count);
        return equationsCreateService.createEquations(CreationOrder.builder()
                .combinedCount(count)
                .build());
    }

    @GetMapping("/fractional/{count}")
    public List<Equation> createFractionalRational(@PathVariable @Positive int count) {
        log.debug("New incoming GET request to create {} fractionalRational equations", count);
        return equationsCreateService.createEquations(CreationOrder.builder()
                .fractionalCount(count)
                .build());
    }

    @GetMapping("/random/{count}")
    public List<Equation> createRandom(@PathVariable @Positive int count) {
        log.debug("New incoming GET request to create {} random equations", count);
        return equationsCreateService.createEquations(CreationOrder.builder()
                .randomCount(count)
                .build());
    }

    @GetMapping("/bundle")
    public List<Equation> createBundleEquations(@RequestBody CreationOrder creationOrder) {
        log.debug("New incoming GET request to create several types of equations: " + creationOrder);
        return equationsCreateService.createEquations(creationOrder);
    }

    @GetMapping("/{id}")
    public Optional<Equation> getEquationsById(@PathVariable @NotEmpty String id) {
        log.debug("New incoming GET request to find equation by ID {}", id);
        return equationRepository.findById(UUID.fromString(id));
    }

    @GetMapping()
    public List<Equation> getEquations() {
        log.debug("New incoming GET request to find all equations");
        return equationRepository.findAll();
    }

    @GetMapping("/type/{type}")
    public List<Equation> getEquationsByType(@PathVariable @NotEmpty String type) {
        log.debug("New incoming GET request to find all equations");
        return equationRepository.findAllByEquationType(Const.EquationType.valueOf(type));
    }

    @ExceptionHandler
    public ResponseEntity<EquationExceptionWrapper> exceptionHandler(Exception exception) {
        final EquationExceptionWrapper exceptionWrapper = new EquationExceptionWrapper(exception);
        if (exception instanceof ConstraintViolationException) {
            return new ResponseEntity<>(exceptionWrapper, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(exceptionWrapper, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
