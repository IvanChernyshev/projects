package student.view;

import javax.swing.*;
import javax.swing.event.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

import student.main.*;
import common.utils.*;
import common.utils.dataObjects.*;
import student.controller.*;

public class AnswerPanel extends AbstractPanel {
	private JList<Task> list;
	private Vector<Task> arrayOfTasks;
	private Task selected;

	public AnswerPanel(UserData userData) {
		super(userData);
	}

	@Override
	protected void setStatic() {
		mainPanel = new JPanel();
		mainPanel.setBounds(0, 0, 384, 471);
		mainPanel.setBackground(Color.lightGray);
		mainPanel.setBorder(TextHandler.getBorder2());
		mainPanel.setLayout(null);

		JPanel topPanel = new JPanel();
		topPanel.setBounds(10, 11, 364, 50);
		mainPanel.add(topPanel);
		topPanel.setLayout(null);
		topPanel.setBackground(Color.lightGray);
		topPanel.setBorder(TextHandler.getBorder1());

		JLabel topLabel = new JLabel("���������� �������");
		topLabel.setBounds(87, 11, 217, 28);
		topLabel.setFont(TextHandler.getBigFont());
		topPanel.add(topLabel);
	}

	@Override
	protected void setDynamic() {
		JPanel lowPanel = new JPanel();
		lowPanel.setBounds(10, 72, 364, 327);
		mainPanel.add(lowPanel);
		lowPanel.setLayout(null);
		lowPanel.setBackground(Color.lightGray);
		lowPanel.setBorder(TextHandler.getBorder1());

		list = new JList<Task>();
		list.setBounds(10, 11, 344, 305);
		list.addListSelectionListener(new ListListener());
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		JScrollPane scroll = new JScrollPane(list);
		scroll.setBounds(10, 11, 344, 305);
		scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		scroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		lowPanel.add(scroll);

		JButton mainButton = new JButton("�� �������");
		mainButton.setBounds(125, 410, 135, 50);
		mainPanel.add(mainButton);
		mainButton.setFont(TextHandler.getLittleFont());
		mainButton.addActionListener(new MainButtonListener());
	}

	public class MainButtonListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			AnswerController.getInstance().returnToMainPanel();
		}
	}

	public class ListListener implements ListSelectionListener {
		@Override
		public void valueChanged(ListSelectionEvent e) {
			if (!e.getValueIsAdjusting()) {
				selected = list.getSelectedValue();
				if (selected != null) {
					AnswerController.getInstance().setTask(selected);
				}
			}
		}
	}

	@Override
	public void update(Observable o, Object arg) {
		if (o instanceof UserData) {
			UserData userData = (UserData) o;
			arrayOfTasks = userData.getArrayOfTasks();
			updateView();
		}
	}

	@Override
	public void updateView() {
		list.setListData(arrayOfTasks);
	}

	@Override
	public void clearAll() {
		deleteTask();
		selected = null;
	}

	private void deleteTask() {
		synchronized (UserData.getInstance()) {
			UserData.getInstance().removeSelectedFromArrayOfTasks();
		}
		updateView();
	}
}
