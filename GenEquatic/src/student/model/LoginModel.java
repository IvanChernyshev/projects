package student.model;

import student.main.*;
import common.utils.dataObjects.*;
import common.constants.*;

public class LoginModel extends AbstractModel {

	public LoginModel(UserData userData) {
		super(userData);
	}

	public void createPerson(LoginDataObject dataObject) {
		String firstName = dataObject.getFirstName();
		String secondName = dataObject.getSecondName();
		String group = dataObject.getGroup();
		String address = getAddress(dataObject.getIpAddress());
		Person person = new Person(firstName, secondName, group, address);
		userData.setPerson(person);
	}

	private String getAddress(String address) {
		if (address == null) {
			address = UtilConstants.DEFAULT_IP_ADDRESS;
		}
		return address;
	}
}
