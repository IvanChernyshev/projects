package common.utils;

public class RandomGenerator {
	public static int getRandom(int min, int max) {
		int random = min + (int) (Math.random() * (max - min) + 1);
		return random;
	}
}
