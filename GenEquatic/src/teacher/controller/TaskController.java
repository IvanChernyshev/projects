package teacher.controller;

import java.util.*;
import java.util.logging.Level;
import javax.swing.*;

import teacher.main.*;
import common.constants.*;
import teacher.model.*;

public class TaskController extends AbstractController {
	private static volatile TaskController instance = null;

	private TaskController() {
	}

	public final static TaskController getInstance() {
		if (instance == null) {
			synchronized (TaskController.class) {
				if (instance == null) {
					instance = new TaskController();
				}
			}
		}
		return instance;
	}

	public void saveTask(List<String> userInput) {
		try {
			validateFormat(userInput);
			selectPanel(PanelsConstants.SAVE_TASK_PANEL);
		} catch (UnsupportedOperationException ex) {
			Log.log(Level.SEVERE, "Not resolved user input text in TaskPanel, method saveTask()", ex);
		} finally {
			clearPanel(PanelsConstants.TASK_PANEL);
		}
	}

	public void startTask(List<String> userInput) {
		try {
			validateFormat(userInput);
			if (UserData.getInstance().isSelfWorking()) {
				selectPanel(PanelsConstants.START_TEST_PANEL);
			} else {
				NetController.getInstance().sendMessage();
				selectPanel(PanelsConstants.TYPE_OF_WORK_PANEL);
			}
		} catch (UnsupportedOperationException ex) {
			Log.log(Level.SEVERE, "Not resolved user input text in TaskPanel, method startTask()", ex);
		} finally {
			clearPanel(PanelsConstants.TASK_PANEL);
		}
	}

	private void validateFormat(List<String> userInput) {
		List<Integer> checkedUserInput = parseUserInput(userInput);
		validateNumber(checkedUserInput);
	}

	private void validateNumber(List<Integer> checkedUserInput) {
		if (findEmptyReplies(checkedUserInput)) {
			JOptionPane.showMessageDialog(null, "����� ����� ������� ���������� �������!");
			throw new UnsupportedOperationException();
		} else {
			TaskModel model = new TaskModel(UserData.getInstance());
			model.setTask(checkedUserInput);
		}
	}

	private boolean findEmptyReplies(List<Integer> numberOfEquations) {
		if (numberOfEquations.get(TaskConstants.LINEAR) <= 0 && numberOfEquations.get(TaskConstants.QUADRATIC) <= 0
				&& numberOfEquations.get(TaskConstants.FRACTIONAL) <= 0
				&& numberOfEquations.get(TaskConstants.COMBAINED) <= 0
				&& numberOfEquations.get(TaskConstants.RANDOM) <= 0) {
			return true;
		}
		return false;
	}

	private int checkMaximumEquations(int numberOfEquations) {
		if (numberOfEquations > 100) {
			numberOfEquations = 100;
		}
		return numberOfEquations;
	}

	private List<Integer> parseUserInput(List<String> userInput) {
		List<Integer> checkedUserInput = new ArrayList<Integer>();
		try {
			for (String input : userInput) {
				int numberOfEquation;
				if (input.equals("")) {
					String defaultInput = "0";
					numberOfEquation = Integer.parseInt(defaultInput);
				} else {
					numberOfEquation = checkMaximumEquations(Integer.parseInt(input));
				}
				checkedUserInput.add(numberOfEquation);
			}
			return checkedUserInput;
		} catch (NumberFormatException ex) {
			Log.log(Level.SEVERE, "User inputed no numbers, method parseUserInput()", ex);
			JOptionPane.showMessageDialog(null, "� ���� ����� ����� �������� ����� ��������� ��������!");
			throw new UnsupportedOperationException();
		}
	}
}
