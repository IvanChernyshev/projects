package com.vano.myTime.model;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

class HtmlParser {

    static String parseHTML(String htmlText) {
        StringBuilder builder = new StringBuilder();
        Document doc = Jsoup.parse(htmlText);
        doc.normalise();
        Elements elements = doc.getElementsByTag("p");
        for (Element el : elements) {
            builder.append(el.text()).append("\n");
        }
        TextParser parser = new TextParser(builder.toString());
        parser.parseText();
        return parser.getResult();
    }
}
